#include<stdio.h>
#include<stdlib.h>
#include"fmi-export/fmi2Functions.h"


void myLogger(fmi2ComponentEnvironment componentEnvironment,
            fmi2String instanceName,
            fmi2Status status,
            fmi2String category,
            fmi2String message,
            ...) { }
void* myCallbackAllocateMemory(size_t nobj, size_t size) { return (void*) malloc(size*nobj);}
void myCallbackFreeMemory(void* obj) { free(obj); }
void myStepFinished(fmi2ComponentEnvironment componentEnvironment,
                    fmi2Status status) { }


int main(){
    printf("FMU Version: %s\n",fmi2GetVersion());
    printf("FMU Platform: %s\n",fmi2GetTypesPlatform());
    
    fmi2CallbackFunctions* callBackFunctions = (fmi2CallbackFunctions*) malloc(sizeof(fmi2CallbackFunctions));
    callBackFunctions->logger = myLogger;
    callBackFunctions->allocateMemory = myCallbackAllocateMemory;
    callBackFunctions->freeMemory = myCallbackFreeMemory;
    callBackFunctions->stepFinished = myStepFinished;
    callBackFunctions->componentEnvironment = NULL;
    
    fmi2Component fmu = fmi2Instantiate( "WP4_Components_Continuous_C1R1_kW",
                						fmi2CoSimulation, 
                						"{826417b1-f4c5-4a0e-aa1f-f6ea67eb0e3d}", 
                						"file:../resources/", 
                						callBackFunctions, 
                						fmi2True, 
                						fmi2False);
    
    double startTime = 0.0;
    double stopTime = 900.0;
    fmi2SetupExperiment(fmu, fmi2False, 0.0, startTime, fmi2True, stopTime);
    fmi2EnterInitializationMode(fmu);
    fmi2ExitInitializationMode(fmu);
    
    double time = startTime;
    fmi2ValueReference server_temp_out[1] = {8};
    fmi2ValueReference server_temp_in[1] = {25};
    fmi2Real valOut[1] = {0};
    while(time < stopTime){
        fmi2Status status = fmi2DoStep(fmu, time, 1, fmi2False);
        if(status == 0){
            fmi2GetReal(fmu, server_temp_out, 1, valOut);
            fmi2SetReal(fmu, server_temp_in, 1, valOut); 
        }
        else{
            printf("doStep failed");
            abort();
        }
        time += 1;
    }
    
    printf("server_temp_out: %lf\n", valOut[0]);
    
    fmi2FreeInstance(fmu);
    
    return 0;
}




