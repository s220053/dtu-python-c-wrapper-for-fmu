from cffi import FFI
import argparse
import shutil
import fnmatch
import os

def ffiBuild(modelIdentifier, path):

    TYPEDEF = '''\
        typedef void*           fmi2Component;
        typedef void*           fmi2ComponentEnvironment;    /* Pointer to FMU environment    */
        typedef void*           fmi2FMUstate;                /* Pointer to internal FMU state */
        typedef unsigned int    fmi2ValueReference;
        typedef double          fmi2Real   ;
        typedef int             fmi2Integer;
        typedef int             fmi2Boolean;
        typedef char            fmi2Char;
        typedef const fmi2Char* fmi2String;
        typedef char            fmi2Byte;
        
        #define fmi2True  1
        #define fmi2False 0
        
        typedef enum {
            fmi2ModelExchange,
            fmi2CoSimulation
        } fmi2Type;
        
        typedef enum {
            fmi2OK,
            fmi2Warning,
            fmi2Discard,
            fmi2Error,
            fmi2Fatal,
            fmi2Pending
        } fmi2Status;
        
        typedef void      (*fmi2CallbackLogger)        (fmi2ComponentEnvironment componentEnvironment,
                                                        fmi2String instanceName,
                                                        fmi2Status status,
                                                        fmi2String category,
                                                        fmi2String message,
                                                        ...);
        typedef void*     (*fmi2CallbackAllocateMemory)(size_t nobj, size_t size);
        typedef void      (*fmi2CallbackFreeMemory)    (void* obj);
        typedef void      (*fmi2StepFinished)          (fmi2ComponentEnvironment componentEnvironment,
                                                        fmi2Status status);
        
        typedef struct {
           fmi2CallbackLogger         logger;
           fmi2CallbackAllocateMemory allocateMemory;
           fmi2CallbackFreeMemory     freeMemory;
           fmi2StepFinished           stepFinished;
           fmi2ComponentEnvironment   componentEnvironment;
        } fmi2CallbackFunctions;
        
        typedef enum {
        	modelInstantiated,
        	modelInitializationMode, /*inside get macro and therfore required to be deifned for fmi1 */
        	modelCsPreEventMode,
        	modelEventMode,
        	modelEventMode2,    /* event iteration ongoing */
        	modelEventModeExit, /* event iteration finished */
        	modelContinousTimeMode,
        	modelTerminated
        } ModelStatus;
        
        typedef struct {
        	fmi2Real time;
        	fmi2Real* parameters;
        	fmi2Real* states;
        	fmi2Real* derivatives;
        	fmi2Real* outputs;
        	fmi2Real* inputs;
        	fmi2Real* auxiliary;
        } ResultValues;
        
        typedef enum {
            fmi2DoStepStatus,
            fmi2PendingStatus,
            fmi2LastSuccessfulTime,
            fmi2Terminated
        } fmi2StatusKind;
        
        
        typedef struct {
           fmi2Boolean newDiscreteStatesNeeded;
           fmi2Boolean terminateSimulation;
           fmi2Boolean nominalsOfContinuousStatesChanged;
           fmi2Boolean valuesOfContinuousStatesChanged;
           fmi2Boolean nextEventTimeDefined;
           fmi2Real    nextEventTime;
        } fmi2EventInfo;
        
    '''
    
    
    FUNCTIONDEF = "fmi2Component " + \
                  modelIdentifier + \
                  "_fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID, fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions, fmi2Boolean visible, fmi2Boolean loggingOn);\n"
                						
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetupExperiment(fmi2Component c, fmi2Boolean relativeToleranceDefined, fmi2Real relativeTolerance, fmi2Real tStart, fmi2Boolean tStopDefined, fmi2Real tStop);\n"
                    
                    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2EnterInitializationMode(fmi2Component c);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2ExitInitializationMode(fmi2Component c);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2Terminate(fmi2Component c);\n"
    
    FUNCTIONDEF += "void " + \
                   modelIdentifier + \
                   "_fmi2FreeInstance(fmi2Component c);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2Reset(fmi2Component c);\n"
    
    
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[]);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2DoStep(fmi2Component c, fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint);\n"
    

   
   
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetTime(fmi2Component c, fmi2Real time);\n"
                   
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2NewDiscreteStates(fmi2Component c, fmi2EventInfo* fmi2eventInfo);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2EnterContinuousTimeMode(fmi2Component c);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetContinuousStates(fmi2Component c, fmi2Real x[], size_t nx);\n"
    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetNominalsOfContinuousStates(fmi2Component c, fmi2Real x_nominal[], size_t nx);\n"
                   
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetDerivatives(fmi2Component c, fmi2Real derivatives[], size_t nx);\n"
                   
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2SetContinuousStates(fmi2Component c, const fmi2Real x[], size_t nx);\n"
   
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2GetEventIndicators(fmi2Component c, fmi2Real eventIndicators[], size_t ni);\n"
                 
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2EnterEventMode(fmi2Component c);\n"
                    
    FUNCTIONDEF += "fmi2Status " + \
                   modelIdentifier + \
                   "_fmi2CompletedIntegratorStep(fmi2Component c, fmi2Boolean noSetFMUStatePriorToCurrentPoint, fmi2Boolean* enterEventMode, fmi2Boolean*  terminateSimulation);\n"
    
    CDEF = TYPEDEF + FUNCTIONDEF
    
    
    SRC = '''\
        #include "fmi-export/fmi2Functions.h"
        #include "simulation/solver/solver_main.h"
        #include "fmi-export/fmu2_model_interface.h"
        #include "simulation_data.h"
        #include "WP4_Components_Continuous_C1R1_kW_functions.h"
        #include "WP4_Components_Continuous_C1R1_kW_literals.h"
    '''
    
    CFILES_ = [  ".c",
                 "_functions.c",
                 "_records.c",
                 "_01exo.c",
                 "_02nls.c",
                 "_03lsy.c",
                 "_04set.c",
                 "_05evt.c",
                 "_06inz.c",
                 "_07dly.c",
                 "_08bnd.c",
                 "_09alg.c",
                 "_10asr.c",
                 "_11mix.c",
                 "_12jac.c",
                 "_13opt.c",
                 "_14lnz.c",
                 "_15syn.c",
                 "_16dae.c",
                 "_17inl.c",
                 "_18spd.c",
                 "_init_fmu.c",
                 "_FMU.c"]
         
    CFILES = [path + modelIdentifier + f for f in CFILES_]
    
    RUNTIMEFILES = ["util/read_matlab4.c",
                    "util/read_csv.c",
                    "util/libcsv.c",
                    "gc/memory_pool.c",
                    "gc/omc_gc.c",
                    "util/base_array.c",
                    "util/boolean_array.c",
                    "util/division.c",
                    "util/doubleEndedList.c",
                    "util/generic_array.c",
                    "util/index_spec.c",
                    "util/integer_array.c",
                    "util/jacobian_util.c",
                    "util/list.c",
                    "util/modelica_string_lit.c",
                    "util/modelica_string.c",
                    "util/ModelicaUtilities.c",
                    "util/omc_error.c",
                    "util/omc_file.c",
                    "util/omc_init.c",
                    "util/omc_mmap.c",
                    "util/omc_msvc.c",
                    "util/omc_numbers.c",
                    "util/parallel_helper.c",
                    "util/rational.c",
                    "util/real_array.c",
                    "util/ringbuffer.c",
                    "util/simulation_options.c",
                    "util/string_array.c",
                    "util/utility.c",
                    "util/varinfo.c",
                    "math-support/pivot.c",
                    "simulation/omc_simulation_util.c",
                    "simulation/options.c",
                    "simulation/simulation_info_json.c",
                    "simulation/simulation_omc_assert.c",
                    "simulation/solver/delay.c",
                    "simulation/solver/fmi_events.c",
                    "simulation/solver/model_help.c",
                    "simulation/solver/omc_math.c",
                    "simulation/solver/spatialDistribution.c",
                    "simulation/solver/stateset.c",
                    "simulation/solver/synchronous.c",
                    "simulation/solver/initialization/initialization.c",
                    "meta/meta_modelica_catch.c"]

    CFILES += [path + f for f in RUNTIMEFILES]
    
    # print(CFILES)
    
    ffibuilder = FFI()
    ffibuilder.cdef(CDEF)
    ffibuilder.set_source(
        modelIdentifier, SRC,
        sources = CFILES,
        include_dirs = [path, path + 'include'],
        libraries = ['m', 'openblas', 'pthread', 'gcc'],
        library_dirs = ["/media/guest_share/fmus/fix4"],
        extra_compile_args = ['-g', '-O2', '-fno-stack-protector',  '-Wno-parentheses-equality', '-Wno-unused-variable' ,'-fPIC'],
        extra_link_args = ['-static-libgcc', '-lm', '-Wl,-Bstatic -lopenblas', '-Wl,-Bdynamic -lm', '-lpthread', '-Wl,-Bstatic -lpthread', '-Wl,-Bdynamic'],
    )
    ffibuilder.compile(verbose = True)
    
    # # List all files in the directory
    # all_files = os.listdir(".")
    # matching_files = [file for file in all_files if fnmatch.fnmatch(file, f"{modelIdentifier}*")]
    # for file in matching_files:
    #     shutil.move(file, "fmuPythonLib/"+file)
    #     # print(file)


ffiBuild("WP4_Components_Continuous_C1R1", "fmuSource/WP4.Components.Continuous.C1R1_kW_Linux/sources/")