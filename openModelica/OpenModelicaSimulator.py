import OMSimulator as oms
import os
    
# print(dir(oms.oms))
oms.setTempDirectory('./temp/')

model = oms.newModel("model")
root = model.addSystem('root', oms.Types.System.SC) # Strongly Coupled system

# instantiate FMUs
root.addSubModel('BaseClasses', 'fmuSource/BouncingBall.fmu')
    
# simulation settings
model.resultFile = 'results.mat'
model.stopTime = 900
model.fixedStepSize = 1

model.instantiate()
# print("server_temp_in:", model.getReal('root.BaseClasses.server_temp_in'))
# model.setReal('root.BaseClasses.server_temp_in', 30)
# print("server_temp_in:", model.getReal('root.BaseClasses.server_temp_in'))

model.initialize()
model.simulate()
model.terminate()
model.delete()