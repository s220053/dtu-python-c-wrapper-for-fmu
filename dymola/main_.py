from fmuBuilder import ffiBuild
from fmuSimulator import FMUWrapper
import argparse
import sys
from datetime import datetime
import zipfile



if __name__ == "__main__":

    # parse input
    parser = argparse.ArgumentParser(description="Build Python Module for FMU")
    parser.add_argument("-n", "--name", required=True, help="ModelIdentifier. For example, WP4_Components_Continuous_C1R1_0kW")
    args = parser.parse_args()
    modelIdentifier = args.name
        
    # unzip fmu        
    zip_file_path = "./fmuSource/" + modelIdentifier + ".fmu"
    extract_to_dir = "./fmuSource/" + modelIdentifier
    
    with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
        zip_ref.extractall(extract_to_dir)
    
    
    
    # build python module
    ffiBuild(modelIdentifier, extract_to_dir)
    print(">> Build " + modelIdentifier + " from " + extract_to_dir + "/sources/")
        


    # simulate
    print(">> Start Simulating...")
    init_time = datetime.now()
    
    loopTimes = 2000000
    fmu = FMUWrapper(modelIdentifier, extract_to_dir, loopTimes)   
   
    
    cooling = {"ATES": 50, "FC": 10, "Chiller": 30}
    start_values={'cooling_demand': 200,
                    'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
                    'server_temp_in': 30,
                    'SERVER_ROOM_TEMP': 25}
                    
    for i in range(loopTimes):
        fmu.initialize(0)
        next = fmu.get_next_server_temperature(start_values)
        fmu.reset()
        # start_values.update({'server_temp_in': next})
        start_values.update({'T0_degC': next})
        if i % 10000 == 0:
            print(i, next)
            
    
    # fmu.reset() # remember to call initialize() before doing simulation

    fin_time = datetime.now()
    print ("Execution time: ", (fin_time-init_time))