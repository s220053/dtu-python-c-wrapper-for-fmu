import importlib
from fmi2Functions import Fmi2Functions
from model_description import read_model_description
 

class FMUWrapper():

    def __init__(self, modelName, path, typeName, **kwargs):
        # read the model description
        self.modelName = modelName
        # import sys
        self.module = importlib.import_module("fmuPythonLib." + modelName)
        self.functions = Fmi2Functions(self.modelName)
        self.model_description = read_model_description(path + "/modelDescription.xml")
        sourcePath = path + "/sources/"  
        
        self.each_simulation_period = 900.0
        self.startTime = 0
        self.stopTime = 0
        self.relative_tolerance = 0
        
        # instantiate the FMU
        if typeName == "CoSimulation":
            self.fmuType = self.module.lib.fmi2CoSimulation
        elif typeName == "ModelExchange":
            self.fmuType = self.module.lib.fmi2ModelExchange
        else:
            raise ValueError("invalid fmu type, CoSimulation or ModelExchange")

        self.fmu = self.functions.fmi2Instantiate(self.module.ffi.new("char[]", self.modelName.encode()), # instanceName
                 self.fmuType, # fmuType: fmi2CoSimulation, ModelExchange
                 self.module.ffi.new("char[]", self.model_description.guid.encode()), # fmuGUID
                 self.module.ffi.new("char[]", sourcePath.encode()), # fmuResourceLocation
                 self.module.ffi.new('fmi2CallbackFunctions *'), # functions
                 True, # visible
                 False) # loggingOn    

 
    def get_next_server_temperature(self, startVals, loopIdx):
        
        if self.fmuType == self.module.lib.fmi2CoSimulation:
            res = self._simulateCS(startVals, loopIdx)
        elif self.fmuType == self.module.lib.fmi2ModelExchange:
            res = self._simulateME(startVals, loopIdx)

        server_temperature = res['server_temp_out']
        return server_temperature
    
    
    def getValInfo(self):
        print(self.model_description.modelVariables)
    
    
    def _simulateCS(self, startVals, loopIdx):
        
        self._setRealVals(startVals)                
        self.startTime = self.each_simulation_period * loopIdx
        self.stopTime = self.each_simulation_period * (loopIdx+1)
        time = self.startTime
        output_interval = 1
        
        self.functions.fmi2SetupExperiment(self.fmu,
                                           False, # toleranceDefined
                                           self.relative_tolerance, # tolerance
                                           self.startTime, # startTime
                                           True, # stopTimeDefined
                                           self.stopTime) # stopTime
        
        self.functions.fmi2EnterInitializationMode(self.fmu)
        self._setRealVal('time_step', self.startTime)
        status = self.functions.fmi2ExitInitializationMode(self.fmu)
        
        if status > 0:
            raise ValueError("initialize failed")
    
        res = { }
        while time < self.stopTime:
            status = self.functions.fmi2DoStep(self.fmu,
                                   time, # currentCommunicationPoint
                                   output_interval, # communicationStepSize
                                   False) # noSetFMUStatePriorToCurrentPoint        
            
            if status == 0: 
                res = self._getRealVals( {'server_temp_out' : 0 } )
                # print(res)
                self._setRealVals( {'server_temp_in' : res['server_temp_out'] } )
            else:
                raise ValueError("doStep failed")
            
            time += output_interval
        
        self._reset()
        return res
        
    
    
    def _simulateME(self, startVals, loopIdx):
        nx = self.model_description.numberOfContinuousStates
        nz = self.model_description.numberOfEventIndicators
        l = 10
        x = self.module.ffi.new("fmi2Real []", l)
        z = self.module.ffi.new("fmi2Real []", l)
        x_nominal = self.module.ffi.new("fmi2Real []", l)
        der_x = self.module.ffi.new("fmi2Real []", l)
        enterEventMode = self.module.ffi.new("fmi2Boolean *")
        terminateSimulation = self.module.ffi.new("fmi2Boolean *")
        eventInfo = self.module.ffi.new("fmi2EventInfo*")
        
        self.startTime = self.each_simulation_period * loopIdx
        self.stopTime = self.each_simulation_period * (loopIdx+1)
        time = self.startTime
        nextTime = self.stopTime
        dt = 1
        eps = 10e4
        
        self.functions.fmi2SetTime(self.fmu, time)
        
        self._setRealVals(startVals)
            
        # initialize
        self.functions.fmi2SetupExperiment(self.fmu,
                                           False, # toleranceDefined
                                           self.relative_tolerance, # tolerance
                                           self.startTime, # startTime
                                           False, # stopTimeDefined
                                           self.stopTime) # stopTime
        
        self.functions.fmi2EnterInitializationMode(self.fmu)
        status = self.functions.fmi2ExitInitializationMode(self.fmu)
        
        if status > 0:
            raise ValueError("initialize failed")

        
        eventInfo.newDiscreteStatesNeeded = True
        while eventInfo.newDiscreteStatesNeeded:
            self.functions.fmi2NewDiscreteStates(self.fmu, eventInfo)
            if eventInfo.terminateSimulation:
                self._close()
        
        
        self.functions.fmi2EnterContinuousTimeMode(self.fmu)
        self.functions.fmi2GetContinuousStates(self.fmu, x, nx)
        self.functions.fmi2GetNominalsOfContinuousStates(self.fmu, x_nominal, nx)
        

        while time < self.stopTime:
        
            self.functions.fmi2GetDerivatives(self.fmu, der_x, nx)
            
            h = min(dt, nextTime - time)
            time += h
            self.functions.fmi2SetTime(self.fmu, time)
            
            for i in range(len(x)):
                x[i] += h * der_x[i]
                
            self.functions.fmi2SetContinuousStates(self.fmu, x, nx)
            self.functions.fmi2GetEventIndicators(self.fmu, z, nz)
            previous_z = z
            
            time_event = abs(time-nextTime) <= eps
            state_event = (previous_z[0] > 0 and z[0]<=0) or (previous_z[0] <= 0 and z[0]>0)
            
            self.functions.fmi2CompletedIntegratorStep(self.fmu, True, enterEventMode, terminateSimulation)
            if terminateSimulation[0]:
                self._close()
            
            
            if enterEventMode[0] or time_event or state_event:
                self.functions.fmi2EnterEventMode(self.fmu)
                
                eventInfo.newDiscreteStatesNeeded = True
                while eventInfo.newDiscreteStatesNeeded:
                    self.functions.fmi2NewDiscreteStates(self.fmu, eventInfo)
                    if eventInfo.terminateSimulation:
                        self._close()
                        
                self.functions.fmi2EnterContinuousTimeMode(self.fmu)
    
                res = self._getRealVals( {'server_temp_out' : 0 } )
                self._setRealVals( {'server_temp_in' : res['server_temp_out'] } )
                
                if eventInfo.valuesOfContinuousStatesChanged == True:
                    self.functions.fmi2GetContinuousStates(self.fmu, x, nx)
                
                if eventInfo.nominalsOfContinuousStatesChanged == True:
                    self.functions.fmi2GetNominalsOfContinuousStates(self.fmu, x_nominal, nx)
                
                if eventInfo.nextEventTimeDefined:
                    nextTime = min(eventInfo.nextEventTime, nextTime)
                else:
                    nextTime = self.stopTime
        
        self._reset()
        return res
    
    
    
    
    def _setRealVals(self, valueDict):
        vs = self.model_description.modelVariables
        for name, val in valueDict.items():
            for v in vs:
                if v.name == name:
                    self._setRealVal(name, val)    
    
    
    def _getRealVals(self, valueDict):        
        vs = self.model_description.modelVariables
        for name in valueDict:
            for v in vs:
                if v.name == name:
                    valueDict[name] = self._getRealVal(name)    
        
        return valueDict
    


    def _setRealVal(self, name, value):
        vs = self.model_description.modelVariables
        for v in vs:
            if v.name == name:
                valRef = self.module.ffi.new("const fmi2ValueReference*")
                valRef[0] = v.valueReference
                realVal = self.module.ffi.new("fmi2Real*")
                realVal[0] = value
                len = 1
                return self.functions.fmi2SetReal(self.fmu, valRef, len, realVal)
        return -1

    def _getRealVal(self, name):
        vs = self.model_description.modelVariables
        for v in vs:
            if v.name == name:
                valRef = self.module.ffi.new("const fmi2ValueReference*")
                valRef[0] = v.valueReference
                realVal = self.module.ffi.new("fmi2Real*")
                len = 1
                self.functions.fmi2GetReal(self.fmu, valRef, len, realVal)
                return realVal[0]

        
    def _reset(self):
        self.functions.fmi2Reset(self.fmu)
        
        
    def _close(self) -> None:
        self.functions.fmi2FreeInstance(self.fmu)


        