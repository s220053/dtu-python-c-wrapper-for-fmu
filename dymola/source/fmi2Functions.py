import importlib


class Fmi2Functions():

    def __init__(self, modelIdentifier):
        self.module = importlib.import_module(modelIdentifier)
        
        self.modelIdentifier = modelIdentifier
        
        
        # Creation and destruction of FMU instances and setting debug status
        self._fmi2Function('fmi2Instantiate',['instanceName', 'fmuType', 'guid', 'resourceLocation', 'callbacks', 'visible', 'loggingOn'],[self.module.ffi.typeof("fmi2String"), self.module.ffi.typeof("fmi2Type"), self.module.ffi.typeof("fmi2String"), self.module.ffi.typeof("fmi2String"), self.module.ffi.typeof("fmi2CallbackFunctions*"), self.module.ffi.typeof("fmi2Boolean"), self.module.ffi.typeof("fmi2Boolean")], self.module.ffi.typeof("fmi2Component"))

        self._fmi2Function('fmi2FreeInstance', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("void"))



        # Enter and exit initialization mode, terminate and reset
        self._fmi2Function('fmi2SetupExperiment', ['component', 'toleranceDefined', 'tolerance', 'startTime', 'stopTimeDefined', 'stopTime'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Boolean"), self.module.ffi.typeof("fmi2Real"), self.module.ffi.typeof("fmi2Real"), self.module.ffi.typeof("fmi2Boolean"), self.module.ffi.typeof("fmi2Real")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2EnterInitializationMode', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2ExitInitializationMode', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2Terminate', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2Reset', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))



        # Getting and setting variable values
        self._fmi2Function('fmi2GetReal', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2Real*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2GetInteger', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2Integer*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2GetBoolean',['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2Boolean*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2GetString',['component', 'vr', 'nvr', 'value'],[self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2String*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2SetReal', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2Real*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2SetInteger', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"),  self.module.ffi.typeof("fmi2Integer*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2SetBoolean', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"),  self.module.ffi.typeof("fmi2Boolean*")], self.module.ffi.typeof("fmi2Status"))

        self._fmi2Function('fmi2SetString', ['component', 'vr', 'nvr', 'value'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2ValueReference*"), self.module.ffi.typeof("size_t"), self.module.ffi.typeof("fmi2String*")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2DoStep', ['component', 'currentCommunicationPoint', 'communicationStepSize', 'noSetFMUStatePriorToCurrentPoint'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real"), self.module.ffi.typeof("fmi2Real"), self.module.ffi.typeof("fmi2Boolean")], self.module.ffi.typeof("fmi2Status"))
                           

        
        # Model Exchange
        self._fmi2Function('fmi2SetTime', ['component', 'time'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2NewDiscreteStates', ['component', 'fmi2eventInfo'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2EventInfo*")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2EnterContinuousTimeMode', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2GetContinuousStates', ['component', 'x', 'nx'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real []"), self.module.ffi.typeof("size_t")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2GetNominalsOfContinuousStates', ['component', 'x_nominal', 'nx'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real []"), self.module.ffi.typeof("size_t")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2GetDerivatives', ['component', 'derivatives', 'nx'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real []"), self.module.ffi.typeof("size_t")], self.module.ffi.typeof("fmi2Status"))

        
        self._fmi2Function('fmi2SetContinuousStates', ['component', 'x', 'nx'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("const fmi2Real []"), self.module.ffi.typeof("size_t")], self.module.ffi.typeof("fmi2Status"))
        
                
        self._fmi2Function('fmi2GetEventIndicators', ['component', 'eventIndicators', 'ni'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real []"), self.module.ffi.typeof("size_t")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2EnterEventMode', ['component'], [self.module.ffi.typeof("fmi2Component")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2SetTime', ['component', 'time'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Real")], self.module.ffi.typeof("fmi2Status"))
        
        self._fmi2Function('fmi2CompletedIntegratorStep', ['component', 'noSetFMUStatePriorToCurrentPoint', 'enterEventMode', 'terminateSimulation'], [self.module.ffi.typeof("fmi2Component"), self.module.ffi.typeof("fmi2Boolean") , self.module.ffi.typeof("fmi2Boolean*") , self.module.ffi.typeof("fmi2Boolean*")], self.module.ffi.typeof("fmi2Status"))
        
                           
                           
    def _fmi2Function(self, functionName, argNames, argTypes, resType):
        f = getattr(self.module.lib, self.modelIdentifier + "_" + functionName)
        setattr(self, functionName,f)