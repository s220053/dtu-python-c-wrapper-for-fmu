import sys
sys.path.append("./source")


from fmuBuilder import ffiBuild
from fmuSimulator import FMUWrapper
import argparse
import sys
from datetime import datetime
import zipfile



if __name__ == "__main__":

    # parse input
    parser = argparse.ArgumentParser(description="Build Python Module for FMU")
    parser.add_argument("-n", "--name", required=True, help="ModelIdentifier. For example, WP4_Components_Continuous_C1R1_0kW")
    parser.add_argument('-m', action='store_true', help='ModelExchange')
    parser.add_argument('-c', action='store_true', help='Colimulation(default)')
    args = parser.parse_args()
    modelIdentifier = args.name
        
    # unzip fmu        
    zip_file_path = "./fmuSource/" + modelIdentifier + ".fmu"
    extract_to_dir = "./fmuSource/" + modelIdentifier
    
    with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
        zip_ref.extractall(extract_to_dir)
    
    
    
    # build python module
    ffiBuild(modelIdentifier, extract_to_dir)
    print(">> Build " + modelIdentifier + " from " + extract_to_dir + "/sources/")
        


    # simulate
    cooling = {"ATES": 50, "FC": 10, "Chiller": 30}
    start_values = {'cooling_demand': 200,
                  'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
                  'server_temp_in': 30,
                  'SERVER_ROOM_TEMP': 25}
                  
    loopTimes = 0
    
    fmuType = "CoSimulation"
    if args.c and args.m:
        print("Both -c and -m options provided. Use CoSimulation as default.")
    elif args.m:
        fmuType = "ModelExchange"
    
    print(">> Start Simulating in", fmuType, "mode ...")
    init_time = datetime.now()
    
    fmu = FMUWrapper(modelIdentifier, extract_to_dir, fmuType)
    # fmu.getValInfo()
   
    next = fmu.get_next_server_temperature(start_values, 0)
    print(next)
    
    for i in range(loopTimes):
        next = fmu.get_next_server_temperature(start_values, i)
        start_values['server_temp_in'] = next
        # if i % 100000 == 0:
        print(i, next)

    fin_time = datetime.now()
    print ("Execution time: ", (fin_time-init_time))    