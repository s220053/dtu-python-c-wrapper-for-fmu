```bash
python ffiBuilder.py -n WP4_Components_Continuous_C1R1_0kW -p ../../resource/codegen_fmus/WP4_Components_Continuous_C1R1_0kW/sources/

gcc -pthread -shared -Wl,-z,relro -g ./WP4_Components_Continuous_C1R1_0kW.o ./../../resource/codegen_fmus/WP4_Components_Continuous_C1R1_0kW/sources/all.o -L/usr/lib64 -lpython3 -o ./WP4_Components_Continuous_C1R1_0kW.cpython-36m-x86_64-linux-gnu.so

python ffiBuilder.py -n WP4_Components_BaseClasses_C0R0_0kW -p ../../resource/codegen_fmus/WP4_Components_BaseClasses_C0R0_0kW/sources/

gcc -pthread -shared -Wl,-z,relro -g ./WP4_Components_BaseClasses_C0R0_0kW.o ./../../resource/codegen_fmus/WP4_Components_BaseClasses_C0R0_0kW/sources/all.o -L/usr/lib64 -lpython3 -o ./WP4_Components_BaseClasses_C0R0_0kW.cpython-36m-x86_64-linux-gnu.so

python3 ffiBuilder.py -n WP4_Components_BaseClasses_C0R0_0kW -p ../../resource/codegen_fmus/WP4_Components_BaseClasses_C0R0_0kW/sources/ 2> /dev/null

python3 buildSimulate.py -n WP4_Components_Continuous_C1R1_0kW -p ../../resource/codegen_fmus/WP4_Components_Continuous_C1R1_0kW/sources/

bash simulate.sh -n WP4_Components_Continuous_C1R1_0kW

```

extra_link_args = ["-lpython3"]


- FMI++, py-fmipp
- cffi, Cython, ctype
- fmi4c
- [modelon-community/fmi-library: C library for importing FMUs](https://github.com/modelon-community/fmi-library)