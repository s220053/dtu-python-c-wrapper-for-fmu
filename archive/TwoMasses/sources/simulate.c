#include "conf.h"
#include "fmiFunctions_fwd.h"
#include "types.h"
#include <stdio.h>

int main(){
    // const char* platform = TwoMasses_fmi2GetTypesPlatform();    
    fmi2String instanceName = "TwoMasses";
    fmi2String guid = "{17f62517-d3fa-44e2-9eae-e2218a143339}";
    fmi2String resourceLocation = "";
    fmi2Boolean visible = fmi2False;
    fmi2Boolean loggingOn = fmi2False;
    
    void callbackLogger(fmi2ComponentEnvironment componentEnvironment,
                        fmi2String instanceName,
                        fmi2Status status,
                        fmi2String category,
                        fmi2String message,
                        ...){;};
    void* callbackAllocateMemory(size_t nobj, size_t size){;};
    void callbackFreeMemory(void* obj){;};
    void stepFinished(fmi2ComponentEnvironment componentEnvironment,
                      fmi2Status status){;};
    void* componentEnvironment = NULL;
    
    fmi2CallbackFunctions function = {
        callbackLogger,
        callbackAllocateMemory,
        callbackFreeMemory,
        stepFinished,
        componentEnvironment
    };
    const fmi2CallbackFunctions* functionPointer = &function;
  
    Component* comp = (Component*) TwoMasses_fmi2Instantiate(  instanceName,
                                                               fmi2CoSimulation,
                                                               guid,
                                                               resourceLocation,
                                                               functionPointer,
                                                               visible,
                                                               loggingOn);
    if(comp == NULL) {;}
    
    fmi2Boolean toleranceDefined = fmi2False;
    fmi2Real tolerance = 0.0;
    fmi2Real startTime = 0.0;
    fmi2Boolean stopTimeDefined = fmi2True;
    fmi2Real stopTime = 10.0;
    TwoMasses_fmi2SetupExperiment((fmi2Component)comp, 
                                  toleranceDefined, 
                                  tolerance, 
                                  startTime, 
                                  stopTimeDefined, 
                                  stopTime);     
    TwoMasses_fmi2EnterInitializationMode((fmi2Component)comp);
    fmi2Status status = TwoMasses_fmi2ExitInitializationMode((fmi2Component)comp);
    
    
    fmi2Real tc = startTime;
    fmi2Real stepSize = 0.01; 
    while((tc < stopTime) && (status == fmi2OK)){
        status = TwoMasses_fmi2DoStep((fmi2Component) comp, tc, stepSize, fmi2True);
        fmi2Boolean terminateSimulation = fmi2False;
        fmi2Boolean boolVal = fmi2False;
        switch (status) {
            case fmi2Discard:
                TwoMasses_fmi2GetBooleanStatus((fmi2Component) comp, fmi2Terminated, &boolVal);
                if (boolVal == fmi2True) printf("Slave TwoMasses wants to terminate simulation.");
                break;
            case fmi2Error:
            case fmi2Fatal:
                terminateSimulation = fmi2True;
            break;
        }
        if (terminateSimulation) break;
        tc += stepSize;
    }
    
    
    if((status != fmi2Error) && (status != fmi2Fatal)){
        TwoMasses_fmi2Terminate((fmi2Component) comp);
    }
    if(status != fmi2Fatal){
        TwoMasses_fmi2FreeInstance((fmi2Component) comp);
    }
    
    return 0;
}