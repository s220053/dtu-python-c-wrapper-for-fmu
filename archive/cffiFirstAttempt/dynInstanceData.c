#include "dynInstanceData.h"

static char dymosimResourceExtraPath[1000];

static const char*dymosimResourceLocation = 
0;

struct DYNInstanceData tempData = {0,0, 0,0,
	1
	,0,{0},{0}
    ,{0.0}
    ,{0},0.0,0.0,1e30,0.0,0.0,1.0,1.0,-1e30,
	0,
#ifdef DymolaInitializeStateFirst_
		  1
#else
		  0
#endif
	  ,0,0,0.0,0,0,0,0, 0,0,0,

	  1.0, 0.0, 0.1, 0, 0, 0.0, 
	  -1e33,-1e33,{{0}}, 
#ifdef NrDymolaTimers_
	  NrDymolaTimers_
#else
	  1
#endif
	  ,{0},{0},
#ifdef DymolaNrStepTimers
	  DymolaNrStepTimers
#else
	  0
#endif
	  ,0,0,0, 0,0,0,0,0,0,0,0,0
};


void DYNInitializeDid(struct DYNInstanceData*did_) {
	if (did_) {
		*did_ = tempData;
	}
}

void dymosimSetResources2(struct DYNInstanceData*did_, const char*s) {
	char* rps = 0;
	size_t sz = 0;
	if (did_) {
		rps = did_->fmiResourceLocation;
		sz = sizeof(did_->fmiResourceLocation);
		rps[0] = '\0';
	}
	else {
		rps = dymosimResourceExtraPath;
		sz = sizeof(dymosimResourceExtraPath);
		rps[0] = '\0';
	}
	if (s && s[0] != '\0' && strlen(s) < sz - 1) {
		{
			char* rp = rps;
			const char* sp = s;
			const char* cp = 0;
			while (cp = strchr(sp, '%')) {
				ptrdiff_t len = cp - sp;
				strncpy(rp, sp, len);
				rp += len;
				sp = cp + 1;
				if (sp[0] && sp[1]) {
					char c[3] = { sp[0] , sp[1], 0 };
					long num = strtol(c, 0, 16);
					if (num) {
						*rp++ = (char)num;
					}
					sp += 2;
				}
			}
			strncpy(rp, sp, sz - (sp - s) - 1);
		}
		rps[sz - 2] = '\0';
		if (rps[strlen(rps) - 1] != '/' && rps[strlen(rps) - 1] != '\\') strcat(rps, "/");
		if (!did_)
			dymosimResourceLocation = rps;
	}
	else {
		if (!did_)
			dymosimResourceLocation = 0;
	}
}