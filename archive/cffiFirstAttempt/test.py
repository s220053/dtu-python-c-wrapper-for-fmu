from _fmi2 import lib, ffi
import pathlib


# convert C string to Python string
to_python_string = lambda c_string: ffi.string(c_string).decode()



# configure fmi2Instantiate() input
instanceName = ffi.new("char[]", "TwoMasses".encode())
fmuType = lib.fmi2CoSimulation
fmuGUID = ffi.new("char[]", "{17f62517-d3fa-44e2-9eae-e2218a143339}".encode())
fmuResourceLocation = ffi.new("char[]", "../../../TwoMasses/".encode())
functions = ffi.new('fmi2CallbackFunctions *')



# instantiate fmu
fmi2Component = ffi.new('fmi2Component')
fmi2Component = lib.fmi2Instantiate(instanceName,
                                    fmuType,
                                    fmuGUID,
                                    fmuResourceLocation,
                                    functions,
                                    1,
                                    0)

print(to_python_string(fmi2Component.instanceName), "instantiated")




# initialize fmu
relativeToleranceDefined = 1
relativeTolerance = 0.0
tStart = 0.0
tStopDefined = 1
tStop = 10.0

status = lib.fmi2SetupExperiment(fmi2Component,
                                 relativeToleranceDefined,
                                 relativeTolerance,
                                 tStart,
                                 tStopDefined,
                                 tStop)
print(status)

status = lib.fmi2EnterInitializationMode(fmi2Component)
print(status)

status = lib.fmi2ExitInitializationMode(fmi2Component)
print(status)



# terminate fmu
status = lib.fmiTerminate(fmi2Component)
print(status)