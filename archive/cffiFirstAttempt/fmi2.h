#ifndef FMI2_HEADER
#define FMI2_HEADER

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

const char* GUIDString;

typedef void*           fmi2ComponentEnvironment;    /* Pointer to FMU environment    */
typedef void*           fmi2FMUstate;                /* Pointer to internal FMU state */
typedef unsigned int    fmi2ValueReference;
typedef double          fmi2Real   ;
typedef int             fmi2Integer;
typedef int             fmi2Boolean;
typedef char            fmi2Char;
typedef const fmi2Char* fmi2String;
typedef char            fmi2Byte;

#define fmi2True  1
#define fmi2False 0

typedef enum {
    fmi2OK,
    fmi2Warning,
    fmi2Discard,
    fmi2Error,
    fmi2Fatal,
    fmi2Pending
} fmi2Status;

typedef enum {
    fmi2ModelExchange,
    fmi2CoSimulation
} fmi2Type;

typedef enum {
    fmi2DoStepStatus,
    fmi2PendingStatus,
    fmi2LastSuccessfulTime,
    fmi2Terminated
} fmi2StatusKind;

typedef void      (*fmi2CallbackLogger)        (fmi2ComponentEnvironment componentEnvironment,
                                                fmi2String instanceName,
                                                fmi2Status status,
                                                fmi2String category,
                                                fmi2String message,
                                                ...);
typedef void*     (*fmi2CallbackAllocateMemory)(size_t nobj, size_t size);
typedef void      (*fmi2CallbackFreeMemory)    (void* obj);
typedef void      (*fmi2StepFinished)          (fmi2ComponentEnvironment componentEnvironment,
                                                fmi2Status status);

typedef struct {
   fmi2CallbackLogger         logger;
   fmi2CallbackAllocateMemory allocateMemory;
   fmi2CallbackFreeMemory     freeMemory;
   fmi2StepFinished           stepFinished;
   fmi2ComponentEnvironment   componentEnvironment;
} fmi2CallbackFunctions;

typedef struct {
   fmi2Boolean newDiscreteStatesNeeded;
   fmi2Boolean terminateSimulation;
   fmi2Boolean nominalsOfContinuousStatesChanged;
   fmi2Boolean valuesOfContinuousStatesChanged;
   fmi2Boolean nextEventTimeDefined;
   fmi2Real    nextEventTime;
} fmi2EventInfo;


#define FMIWarning				fmi2Warning
#define FMIDiscard				fmi2Discard
#define FMIError				fmi2Error
#define FMIFatal				fmi2Fatal
#define FMIPending				fmi2Pending
#define FMIStatus				fmi2Status
#define FMIModelExchange        fmi2ModelExchange
#define FMICoSimulation			fmi2CoSimulation
#define FMIType					fmi2Type

#define FMIComponent			fmi2Component              
#define FMICompoenetEnvironment fmi2ComponentEnvironment
#define FMIReal					fmi2Real
#define FMIInteger				fmi2Integer
#define FMIFMUstate				fmi2FMUstate  
#define FMIBoolean				fmi2Boolean
#define FMIChar					fmi2Char
#define FMIString				fmi2String
#define FMIByte					fmi2Byte
#define FMITrue					fmi2True	
#define FMIFalse				fmi2False
#define FMIValueReference		fmi2ValueReference

#define FMICallbackLogger			fmi2CallBackLogger
#define FMICallbackAllocateMemory	fmi2CallbackAllocateMemory
#define FMICallbackFreeMemory		fmi2CallbackFreeMemory
#define FMIStepFinished				fmi2StepFinished
#define FMICallbackFunctions		fmi2CallbackFunctions
#define FMIEventInfo			fmi2EventInfo
#define FMIOK					fmi2OK

typedef struct {
	int nJacA;
	int nJacB;
	int nJacC;
	int nJacD;
	double * jacA;
	double * jacB;
	double * jacC;
	double * jacD;

	size_t nJacV;
	size_t nJacZ;
	double* jacV;
	double* jacVTmp1;
	double* jacVTmp2;
	double* jacZ;
	double* jacZTmp1;
	double* jacZTmp2;

	/*Temporary solution for discreteStates */
	fmi2FMUstate stateStore; 
} JacobianData;

typedef enum {
	modelInstantiated,
	modelInitializationMode, /*inside get macro and therfore required to be deifned for fmi1 */
	modelCsPreEventMode,
	modelEventMode,
	modelEventMode2,    /* event iteration ongoing */
	modelEventModeExit, /* event iteration finished */
	modelContinousTimeMode,
	modelTerminated
} ModelStatus;

typedef struct {
	fmi2Real time;
	fmi2Real* parameters;
	fmi2Real* states;
	fmi2Real* derivatives;
	fmi2Real* outputs;
	fmi2Real* inputs;
	fmi2Real* auxiliary;
} ResultValues;

typedef struct {
	// struct DeclarePhase phase;
	/* last time for sample, possibly buffered */
	fmi2Real lastSampleTime;
	/* last time for sample on file */
	fmi2Real lastFileSampleTime;
	/* last neglected sample warning applies up to this time */
	fmi2Real lastWarnTime;
	/* buffered values */
	ResultValues** buf;
	int curBufSize;
	int curStartIx;
	int curIx;
	int nUsed;
	/* whether have warned about forced flush */
	fmi2Boolean flushWarned;
	unsigned long long resultSampleCounter;
} Result;

typedef struct tpl_bin {
    void *addr;
    uint32_t sz;
} tpl_bin;

typedef struct {
	fmi2Real tStart;
	fmi2Boolean StopTimeDefined;
	fmi2Real tStop;
	fmi2Boolean relativeToleranceDefined;
    fmi2Real relativeTolerance;
    
	fmi2Boolean isCoSim;

	fmi2String instanceName;
	fmi2Boolean loggingOn;
	fmi2Boolean loggFuncCall;

	size_t nStates;
	size_t nIn;
	size_t nOut;
	size_t nAux;
	size_t nPar;
	size_t nSPar;
	size_t nCross;
	size_t nDStates;
	size_t nPrevious;
	size_t nConstAux;

	fmi2Real* states;
	fmi2Real* parameters;
	fmi2Real* derivatives;
	fmi2Real* inputs;
	fmi2Real* outputs;
	fmi2Real* auxiliary;
	fmi2Real* crossingFunctions;
	fmi2Real* dStates;
	fmi2Real* previousVars;

	fmi2Real* statesNominal;

	fmi2Char** sParameters;
	fmi2Char** tsParameters;

	fmi2Real* oldStates; /*To check if state values have changed durin event*/

	const fmi2CallbackFunctions* functions;
	
	fmi2Real time;
	int icall;

	/* for buffering of short messages without linebreaks */
	char logbuf[256];
	char* logbufp;

	ModelStatus mStatus;
	/* only for FMI 2.0 really, but convenient internally also for 1.0 */
	fmi2Boolean terminationByModel;

	/* for storing of result from within FMU */
	fmi2Boolean storeResult;
	Result* result;

	struct BasicDDymosimStruct* dstruct;
	struct BasicIDymosimStruct* istruct;
	/* to avoid numerous castings */
	double* duser;
	int* iuser;

	/* for internal integration */
	// IntegrationData* iData;

	int allocDone;
	int valWasSet;
	int eventIterRequired;
	int recalJacobian;
	int enforceRefresh; /*Temp due to idemand bug for discret states*/
	int hycosimInputEvent;

	/* for fmiGetDirectionalDerivative */
	JacobianData jacData;
	fmi2Boolean firstEventCall;
	int eventIterationOnGoing;
	void**handles;
	int QiErr;
	struct DYNInstanceData* did;
	unsigned long long inlineStepCounter;
	double nextResultSampleTime;

	fmi2Boolean AdvancedDiscreteStates;
	fmi2Boolean* ClockZeroVector;
	
	tpl_bin* tplDidBin;
	double* serializedDelayData;

	fmi2Boolean anyNonEvent;
	fmi2Boolean steadyStateReached;

	fmi2Boolean fmi2ComputeInit;

	fmi2Boolean maxRunTimeReached;
	double oneSimulationTotalTimerStart;
} Component;

typedef Component*       fmi2Component;               /* Pointer to FMU instance       */

#define NULL ((void *)0)

const char* loggFuncCall;
const char* loggUndef;
const char* loggQiErr;
const char* loggBadCall;
const char* loggSundials;
const char* loggMemErr;
const char* loggStats;



struct BasicIDymosimStruct {
	int mFindEvent;
	int mPrintEvent;
	int mallowSingularFlag;
	int mInJacobian;
	int mSolverHandleEq;
	int mInlineIntegration;
	int mTriggerStepEvent;

	int mDymolaStoreAuxiliaries;

	int mParametersNr;
	int mEvent;
	int mContinueSimulate;

	int mAtDAEEvent;
	int mRememberOnlyAccepted;

	int mTriggerResultSnapshot;

	int mUsingCVodeGodess;

	int mBlockUnblockSmoothCrossings;
};

struct BasicDDymosimStruct {
	double mDymolaFixedStepSize;
	double mCurrentStepSizeRatio2;
	double mNextTimeEvent;
	double mOrigTimeError;
	double mStoreResultInterval;
	double mStartNextTimeEvent;
};


#define NX_    2
#define NX2_   0
#define NU_    1
#define NY_    1
#define NW_    7
#define NWP_   2
#define NP_    5
#define NPS_   0
#define ND_   0
#define NXP_   0
#define NRel_  0
#define NRelF_  0
#define NCons_ 0
#define DAEsolver_ 0  

void GetDimensions4(int *nx_, int *nx2_, int *nu_, int *ny_, int *nw_, int *np_, int* nsp_,
int*nrel2_,int *nrel_, int *ncons_, int *dae_, int *nd_, int* nxp_, int* nwp_);

#define MAXAuxStrLen_   500

size_t DYNGetMaxAuxStrLen();

unsigned int FMIClockValueReferences_[1];

fmi2Component fmi2Instantiate(fmi2String instanceName,
						fmi2Type fmuType, 
						fmi2String fmuGUID, 
						fmi2String fmuResourceLocation, 
						const fmi2CallbackFunctions* functions, 
						fmi2Boolean visible, 
						fmi2Boolean loggingOn);
						
fmi2Component fmiInstantiate_(fmi2String instanceName, 
							  fmi2Type fmuType,
							  fmi2String fmuGUID,
							  fmi2String fmuResourceLocation,
							  const fmi2CallbackFunctions* functions,
							  fmi2Boolean visible,
							  fmi2Boolean loggingOn);


fmi2Component fmiInstantiateSlave_(fmi2String instanceName,
								fmi2String fmuGUID,
								fmi2String fmuResourceLocation,
								const fmi2CallbackFunctions* functions,
								fmi2Boolean visible,
								fmi2Boolean loggingOn);


fmi2Component fmiInstantiateModel_(fmi2String instanceName,
								  fmi2String fmuGUID,
								  fmi2String fmuResourceLocation,
								  const fmi2CallbackFunctions* functions,
								  fmi2Boolean visible,
								  fmi2Boolean loggingOn);

fmi2String util_strdup(fmi2String s);

int fmiUser_Instantiate();

#define ALLOCATE_MEMORY(nbr, size) calloc((nbr), (size))
#define FREE_MEMORY(ptr) do {free((void*) (ptr)); (ptr) = 0;}while(0)

// void declareNew2_(double x0_[], double dx0_[], double dp_[], double du_[], double dy_[], double dw_[], double nom_[], void*cuser_[], int *QiErr, int setDefault_, struct DeclarePhase* phase)
// {
// 	int setDefaultX_=0,setDefaultU_=0,setDefaultY_=0,setDefaultP_=0,setDefaultDX_=0,setDefaultW_=0;
// }

extern size_t dyn_allowMultipleInstances;
extern size_t dyn_instanceDataSize;

void setBasicStruct(double*d,int*i);



// for logging
void util_logger(Component* comp, 
                 fmi2String instanceName,
                 fmi2Status status,
				 fmi2String category, 
				 fmi2String message, ...);
				 

#define _MSC_VER 900
extern const char* loggFuncCall;
extern const double cvodeTolerance;


typedef enum {
	iDemandStart,            /* Start of integration: Initial equations are solved. */
	iDemandOutput,           /* Compute outputs */
	iDemandDerivative,       /* Compute derivatives */
	iDemandVariable,         /* Compute auxiliary variables */
	iDemandCrossingFunction, /* Compute crossing functions */
	iDemandEventHandling,    /* Event handling */
	iDemandTerminal = 7      /* 'terminal()' is true. */
} IDemandLevel;


#endif