#include "fmiInitialize.h"
#include "dynInstanceData.h"
#include "stdio.h"
#include <stdarg.h>

const double cvodeTolerance = 1E-05;

void util_logger(Component* comp, FMIString instanceName, FMIStatus status,
							   FMIString category, FMIString message, ...)
{
	char buf[1024];
	va_list ap;
	int capacity;
	int n = 0;

	if (category == loggFuncCall && !comp->loggFuncCall || comp->loggingOn == FMIFalse && (status < FMIWarning)) {
		return;
	}
	
	capacity = sizeof(buf) - 1;
	if (comp->logbufp > comp->logbuf) {
		/* add buffered messages */
#if defined(_MSC_VER) && _MSC_VER>=1400
		n = _snprintf_s(buf, capacity, _TRUNCATE, "%s", comp->logbuf);
#else
		buf[capacity]=0;
		n = snprintf(buf, capacity, "%s", comp->logbuf);
#endif
		if (n >= capacity || n <= 0) {
			goto done;
		}
	}
	va_start(ap, message);
#if defined(_MSC_VER) && _MSC_VER>=1400
	vsnprintf_s(buf + n, capacity - n, _TRUNCATE, message, ap);
#else
	buf[capacity]=0;
	vsnprintf(buf + n, capacity - n, message, ap);
#endif
	va_end(ap);
	comp->functions->logger(comp->functions->componentEnvironment, instanceName, status, category, "%s", buf);

done:
	comp->logbufp = comp->logbuf;
}


fmi2Status fmi2SetupExperiment(fmi2Component c,
								fmi2Boolean relativeToleranceDefined,
								fmi2Real relativeTolerance,
								fmi2Real tStart,
								fmi2Boolean tStopDefined,
								fmi2Real tStop)
{
	static fmi2String label = "fmi2SetupExperiment";
	Component* comp = (Component*) c;
	comp->tStart = tStart;
	comp->StopTimeDefined = tStopDefined;
	comp->tStop = tStop;
	if(relativeToleranceDefined && !comp->isCoSim ){
		util_logger(comp, comp->instanceName, fmi2Warning, loggUndef, "%s: tolerance control not supported for fmuType fmi2ModelExchange, setting toleranceDefined to fmi2False",label);
		comp->relativeToleranceDefined = fmi2False;
		comp->relativeTolerance =0;
	}else{
		comp->relativeToleranceDefined = relativeToleranceDefined;
		comp->relativeTolerance = relativeTolerance;
	}
	comp->time = tStart;
	comp->icall = iDemandStart;
	return fmi2OK;
}


fmi2Status fmi2EnterInitializationMode(fmi2Component c)
{
	Component* comp = (Component*) c;
	// if (fmiUser_Initialize())
	// 	return util_error(comp);
	if (comp->mStatus != modelInstantiated) {
		util_logger(comp, comp->instanceName, fmi2Error, loggBadCall, "model cannot be initialized in current state(%d)", comp->mStatus);
		return util_error(comp);
	}
	comp->fmi2ComputeInit = fmi2True;
	comp->mStatus = modelInitializationMode;
	return fmi2OK;
}



fmi2Status fmi2ExitInitializationMode(fmi2Component c)
{
	Component* comp = (Component*) c;
	if(comp->isCoSim){
		return fmiExitSlaveInitializationMode_(comp);
	}else if(!comp->isCoSim){
		return fmiExitModelInitializationMode_(comp);
	}
	return util_error(comp);
}



fmi2Status fmiExitSlaveInitializationMode_(fmi2Component c)
{
	static fmi2String label = "fmiExitInitializationMode";
	Component* comp = (Component*) c;
	fmi2Status status;
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s...", label);
	status = util_exit_model_initialization_mode(c, label, modelContinousTimeMode);
	if (status != fmi2OK) {
		return status;
	}
	comp->mStatus = modelContinousTimeMode;
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s completed", label);
	return fmi2OK;
}

fmi2Status fmiExitModelInitializationMode_(fmi2Component c)
{
	static fmi2String label = "fmi2ExitInitializationMode";
	Component* comp = (Component*) c;
	fmi2Status status;

	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s...", label);
	status = util_exit_model_initialization_mode(c, label, modelEventMode);
	comp->firstEventCall=fmi2True;
	if (status != fmi2OK) {
		return status;
	}
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s completed", label);
	return fmi2OK;
}


fmi2Status util_exit_model_initialization_mode(fmi2Component c, const char* label, ModelStatus nextStatus)
{
	Component* comp = (Component*) c;
	int QiErr;
	
	if (comp->mStatus != modelInitializationMode) {
		util_logger(comp, comp->instanceName, fmi2Error, loggBadCall, "%s: may only called in initialization mode", label);
		return util_error(comp);
	}

	SetDymolaOneIteration(comp->did, 4);
	QiErr = util_refresh_cache(comp, iDemandStart, NULL, NULL);
	if (QiErr != 0) {
		return util_error(comp);
	}
	/* reset */
	SetDymolaOneIteration(comp->did, 0);
	// if (comp->storeResult == fmi2True) {
	// 	result_sample(comp, fmi2True); 
	// }
	comp->mStatus = nextStatus;
	return fmi2OK;
}



void SetDymolaOneIteration(struct DYNInstanceData*did_, int val) {
	if (did_) did_->DymolaOneIteration_var = val;
	else tempData.DymolaOneIteration_var = val;
}


int GetDymolaOneIteration(struct DYNInstanceData*did_) {
	if (did_) return did_->DymolaOneIteration_var;
	return tempData.DymolaOneIteration_var;
}


int util_refresh_cache(Component* comp, int idemand, const char* label, fmi2Boolean* iterationConverged)
{
	if(comp->fmi2ComputeInit){
		fmi2Status ret = fmi2OK;
		int saveDymolaOneIteration = GetDymolaOneIteration(comp->did);
		int saveIcall = comp->icall;
		comp->fmi2ComputeInit = fmi2False;
		if(comp->isCoSim){
			ret = fmiEnterSlaveInitializationMode_(comp, comp->relativeTolerance, comp->tStart, comp->StopTimeDefined, comp->tStop);
		}else if(!comp->isCoSim){
			ret = fmiEnterModelInitializationMode_(comp, FMIFalse, 0);
		}
		if(ret != fmi2OK)
			return ret; /*Shold be error or fatal;*/
		comp->icall=saveIcall;
		SetDymolaOneIteration(comp->did,saveDymolaOneIteration);
	}
	switch (comp->mStatus) {
	    case modelInstantiated:
			if (GetDymolaOneIteration(comp->did) == 0) {
				SetDymolaOneIteration(comp->did, 5);
			}
			break;
		case modelInitializationMode:
			if (GetDymolaOneIteration(comp->did) == 0) {
				SetDymolaOneIteration(comp->did, 5);
			}
			break;
		case modelEventMode:
		case modelEventMode2:
		case modelEventModeExit:
			if(comp->firstEventCall){
				SetDymolaOneIteration(comp->did, 2);
				comp->firstEventCall=FMIFalse;
			}else if (GetDymolaOneIteration(comp->did) == 0) {
				SetDymolaOneIteration(comp->did, 5);
			}
			idemand=iDemandEventHandling;
			break;
		default:
			/*assert(GetDymolaOneIteration(comp->did) == 0);*/
			break;
	}

	comp->QiErr=0;
	copyDStatesToDid(comp->did,comp->dStates,comp->previousVars);
	if (comp->did) {
		globalComponent2=comp;
		dsblock_did(&idemand, &comp->icall, &comp->time, comp->states, 0,
			comp->inputs, comp->parameters, 0, 0, comp->derivatives, comp->outputs, comp->auxiliary,                                
			comp->crossingFunctions, comp->duser, comp->iuser, (void**) comp->sParameters, comp->did, &comp->QiErr);
		globalComponent2=0;
	} else {
		dsblock_(&idemand, &comp->icall, &comp->time, comp->states, 0,             
			comp->inputs, comp->parameters, 0, 0, comp->derivatives,       
			comp->outputs, comp->auxiliary,                                
			comp->crossingFunctions, comp->duser, comp->iuser, (void**) comp->sParameters, &comp->QiErr);
	}
	copyDStatesFromDid(comp->did,comp->dStates,comp->previousVars);
	/*Only check convergence criteria for functions that need it i.e. event handling*/
	if(iterationConverged){
		*iterationConverged = (GetDymolaOneIteration(comp->did)  == 0) ? FMITrue : FMIFalse;
	}
	/*Always reset*/
	SetDymolaOneIteration(comp->did, 0);
	comp->valWasSet = 0;
	if (idemand == 5 && (comp->QiErr == -995 || comp->QiErr == -993)) {
		comp->anyNonEvent = fmi2True;
	} else {
		comp->anyNonEvent = fmi2False;
	}
	if (comp->QiErr>=-995 && comp->QiErr<=-990) comp->QiErr=0; /* Ignore special cases for now, except efficient minor events */
	if (comp->QiErr == 0) {
		return 0;
	} 
	
	if (label != NULL) {
		util_logger(comp, comp->instanceName, fmi2Error, loggQiErr,                     
			"%s: dsblock_ failed, QiErr = %d", label, comp->QiErr);
	}
	return comp->QiErr;
}




fmi2Status fmiEnterSlaveInitializationMode_(fmi2Component c,
                                            fmi2Real relativeTolerance,
											fmi2Real tStart,
											fmi2Boolean StopTimeDefined,
											fmi2Real tStop)
{
	static fmi2String label = "fmi2EnterInitializationMode";
	Component* comp = (Component*) c;
	fmi2Status status;
	if(!comp->relativeToleranceDefined){
		relativeTolerance = cvodeTolerance;
	}
	status = util_initialize_slave(c, relativeTolerance, tStart, StopTimeDefined, tStop);
	if (status != fmi2OK) {
		return status;
	}
	if(comp->istruct->mInlineIntegration && tStart> 0.0 && comp->dstruct->mDymolaFixedStepSize > 0.0){
		double h = comp->dstruct->mDymolaFixedStepSize;
		double quota = tStart/h;
		comp->inlineStepCounter =  (unsigned long long) quota;
	}else{
		comp->inlineStepCounter = 0;
	}
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s", label);
	return status;
}


fmi2Status fmiEnterModelInitializationMode_(fmi2Component c, fmi2Boolean toleranceControlled, fmi2Real relativeTolerance)
{
	static fmi2String label = "fmi2EnterInitializationMode";
	Component* comp = (Component*) c;
	fmi2Status status;

	util_logger(comp, comp->instanceName, FMIOK, loggFuncCall, "%s...", label);

	status = util_initialize_model(c, toleranceControlled, relativeTolerance, fmi2False);
	if (status != fmi2OK) {
		return status;
	}
	comp->mStatus = modelInitializationMode;
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s completed", label);
	return status;
}



void InitializeDymosimStruct(struct BasicDDymosimStruct*basicD,struct BasicIDymosimStruct*basicI) {
#if INLINE_INTEGRATION
	  basicI->mInlineIntegration=INLINE_INTEGRATION;
#else
	  basicI->mInlineIntegration=0;
#endif
	  basicI->mUsingCVodeGodess = 0;
	  basicI->mBlockUnblockSmoothCrossings = 0;
#if defined(DymolaGeneratedFixedStepSize_)
	 basicD->mDymolaFixedStepSize=DymolaGeneratedFixedStepSize_;
#else
	 basicD->mDymolaFixedStepSize=0.0;
#endif
	 basicD->mCurrentStepSizeRatio2 = 1.0;
	 basicD->mOrigTimeError=0;
#if defined(FMUStoreResultInterval_)
	 basicD->mStoreResultInterval = FMUStoreResultInterval_;
#else
	 basicD->mStoreResultInterval = 0;
#endif
}


void SetDymolaEventOptional(struct DYNInstanceData*did_, int val) {
	if (did_) did_->DymolaEventOptional_var=val;
	else tempData.DymolaEventOptional_var=val;
}



fmi2Status util_initialize_slave(fmi2Component c,
                                 fmi2Real relativeTolerance,
								 fmi2Real tStart,
								 fmi2Boolean StopTimeDefined,
								 fmi2Real tStop)
{
	fmi2Status status;
	fmi2Real relTol = relativeTolerance;
	Component* comp = (Component*) c;

	status = util_initialize_model(c, fmi2False, 0, fmi2False);
	
	if (status != fmi2OK) {
		return status;
	}
	comp->StopTimeDefined = StopTimeDefined;
	comp->tStop = tStop;
	comp->valWasSet = 0;
	return fmi2OK;
}


fmi2Status util_initialize_model(fmi2Component c, fmi2Boolean toleranceControlled, fmi2Real relativeTolerance, fmi2Boolean complete)
{
	Component* comp = (Component*) c;
	fmi2Status status = fmi2OK;
	int QiErr = 0;
	/* Start of integration */
	int idemand = iDemandStart;
	comp->terminationByModel = fmi2False;
	
	/* Ignore toleranceControlled for external integration according to recommendation from Hans Olsson:
	The tolerance for the equations to be solved at events are based on the crossing function
	tolerance (to avoid chattering), which is normally stricter than relativeTolerance.
	Hence, relativeTolerance is normally not significant. */
	if (toleranceControlled == fmi2True) {
		util_logger(comp, comp->instanceName, fmi2Warning, loggBadCall, "fmiInitialize: tolerance control not supported");
	}
	InitializeDymosimStruct(comp->dstruct, comp->istruct);
#ifdef OVERIDE_DYMOLA_EVENT_OPTIONAL
	SetDymolaEventOptional(comp->did,  1);
#else
  #ifdef ONLY_INCLUDE_INLINE_INTEGRATION
	if(comp->isCoSim){
		SetDymolaEventOptional(comp->did,  1);
	}else{
		SetDymolaEventOptional(comp->did,  0);
	}
  #else
	if(comp->isCoSim && comp->istruct->mInlineIntegration){
			SetDymolaEventOptional(comp->did,  1);
			comp->istruct->mFindEvent = 1;
	}else{
			SetDymolaEventOptional(comp->did,  0);
	}
  #endif
#endif
	comp->icall = iDemandStart - 1;
	if (complete == fmi2False) {
		SetDymolaOneIteration(comp->did, 2);
	} else {
		SetDymolaOneIteration(comp->did, 0);
	}
	QiErr = util_refresh_cache(comp, idemand, NULL, NULL);
	if (QiErr != 0) {
		status = fmi2Error;
		util_logger(comp, comp->instanceName, status, loggQiErr,
			"fmiInitialize: dsblock_ failed, QiErr = %d", QiErr);
		util_logger(comp, comp->instanceName, status, loggUndef,
			"Unless otherwise indicated by error messages, possible errors are (non-exhaustive):\n"
			"The model references external data that is not present on the target machine, at least not with the same location.\n");
		return util_error(comp);
	} 

	if (comp->storeResult == FMITrue) {
		if(result_setup(comp)){
			status = FMIFatal;
			util_logger(comp, comp->instanceName, status, loggUndef,
			"fmiInitialize: Error when initializing result file, out of memory\n");
			return status;
		}
	}

	return status;
}




fmi2Status util_error(Component* comp)
{	
	/*Termination should be done exteranly when error is thrown*/
	switch(comp->mStatus) {
	    case modelInstantiated:
			/* internally state "error" and "terminated" are equivalent */
			comp->mStatus = modelTerminated;
			break;
		case modelTerminated:
			break;
		default:
			fmiTerminateModel_(comp);
			break;
	}
	return fmi2Error;
}


fmi2Status fmiTerminate(fmi2Component c)
{
	Component* comp = (Component*) c;
	if(comp->isCoSim){
		return fmiTerminateSlave_(comp);
	}else if(!comp->isCoSim){
		return fmiTerminateModel_(comp);
	}
	return util_error(comp);
}




fmi2Status fmiTerminateSlave_(fmi2Component c)
{	

	static fmi2String label = "fmi2Terminate";
	Component* comp = (Component*) c;
	fmi2Status status =fmi2OK;
	fmi2Status status2 = fmi2OK;
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s...", label);
	if (comp->mStatus == modelTerminated) {
		util_logger(comp, comp->instanceName, fmi2Warning, loggBadCall, "%s: already terminated, ignoring call", label);
		return fmi2Warning;
	}
	// if(fmiUser_Terminate())
	// 	return util_error(comp);
	if(comp->QiErr == 0 && comp->terminationByModel == fmi2False){
		/*Special case for terminal, call dsblock_ directly instead of
		using util_refresh_cache to avoid messy logic*/
		int terminal = iDemandTerminal;
		copyDStatesToDid(comp->did,comp->dStates,comp->previousVars);
		if (comp->did) {
			globalComponent2=comp;
			dsblock_did(&terminal, &comp->icall, &comp->time, comp->states, 0,
			comp->inputs, comp->parameters,
			0, 0, comp->derivatives, comp->outputs, comp->auxiliary,                                
			comp->crossingFunctions, comp->duser, comp->iuser,
			(void**) comp->sParameters, comp->did, &comp->QiErr);
			globalComponent2=0;
		} else {
			dsblock_(&terminal, &comp->icall, &comp->time, comp->states, 0,             
			comp->inputs, comp->parameters, 0, 0, comp->derivatives,       
			comp->outputs, comp->auxiliary,                                
			comp->crossingFunctions, comp->duser, comp->iuser,
			(void**) comp->sParameters, &comp->QiErr);
		}
		copyDStatesFromDid(comp->did,comp->dStates,comp->previousVars);
		if (comp->QiErr>=-995 && comp->QiErr<=-990) comp->QiErr=0; /* Ignore special cases for now */
		if(!(comp->QiErr == 0 || comp->QiErr==-999)){
			status = fmi2Error;
			util_logger(comp, comp->instanceName, fmi2Error, loggQiErr,
				"%s: calling terminal section of dsblock_ failed, QiErr = %d",
				label,comp->QiErr);
		}
	}
// #ifndef ONLY_INCLUDE_INLINE_INTEGRATION
// 	if(!comp->istruct->mInlineIntegration){
// 		if(integration_teardown(c) == integrationFatal)
// 			return fmi2Fatal;
// 	}
// #endif
	util_print_dymola_timers(c);
// #ifndef FMU_SOURCE_CODE_EXPORT
// 	if (comp->storeResult == fmi2rue) {
// 		if(result_teardown(comp)){
// 			status = FMIFatal;
// 			util_logger(comp, comp->instanceName, status, loggUndef,
// 				"%s: Error when terminating result file, out of memory\n", label);
// 			return status;
// 		}
		
// 	}
// #endif /* FMU_SOURCE_CODE_EXPORT */

	// if (GetAdditionalFlags(9) && GetAdditionalFlags(10) && !comp->steadyStateReached) {
	// 	status = fmi2Error;
	// 	util_logger(comp, comp->instanceName, status, loggUndef,
	// 		"%s: The stop time was reached before a steady state was detected\n", label);
	// }

	comp->mStatus = modelTerminated;
	util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s completed", label);
	return status;
}




fmi2Status fmiTerminateModel_(fmi2Component c)
{
	static fmi2String label = "fmi2Terminate";
	Component* comp = (Component*) c;
	fmi2Status status = fmi2OK;
	if (comp->mStatus == modelTerminated) {
		util_logger(comp, comp->instanceName, FMIError, loggBadCall, "%s: already terminated, ignoring call", label);
		return util_error(comp);
	}

	if (!comp->isCoSim) {
		util_logger(comp, comp->instanceName, fmi2OK, loggFuncCall, "%s",label);
		if(comp->QiErr == 0 && comp->terminationByModel == fmi2False){
			/*Special case for terminal, call dsblock_ directly instead of
			using util_refresh_cache to avoid messy logic*/
			int terminal = iDemandTerminal;
			copyDStatesToDid(comp->did,comp->dStates,comp->previousVars);
			if (comp->did) {
				globalComponent2=comp;
				dsblock_did(&terminal, &comp->icall, &comp->time, comp->states, 0,
					comp->inputs, comp->parameters, 0, 0, comp->derivatives, 
					comp->outputs, comp->auxiliary,                                
					comp->crossingFunctions, comp->duser, comp->iuser,
					(void**) comp->sParameters, comp->did, &comp->QiErr);
				globalComponent2=0;
			} else {
				dsblock_(&terminal, &comp->icall, &comp->time, comp->states, 0,             
					comp->inputs, comp->parameters, 0, 0, comp->derivatives,       
					comp->outputs, comp->auxiliary,                                
					comp->crossingFunctions, comp->duser, comp->iuser,
					(void**) comp->sParameters, &comp->QiErr);
			}
			copyDStatesFromDid(comp->did,comp->dStates,comp->previousVars);
			if (comp->QiErr>=-995 && comp->QiErr<=-990) comp->QiErr=0; /* Ignore special cases for now */
			if(!(comp->QiErr == 0 || comp->QiErr==-999)){
				status = fmi2Error;
				util_logger(comp, comp->instanceName, fmi2Error, loggQiErr,
					"%s: calling terminal section of dsblock_ failed, QiErr = %d",
					label,comp->QiErr);
			}
		}
	}
	util_print_dymola_timers(c);
	comp->mStatus = modelTerminated;
	return status;
}


void copyDStatesToDid(struct DYNInstanceData*did_, double* dStates, double* previousVars){
	if(!did_){
		did_=&tempData;
	}
	memcpy(did_->discreteState_var, dStates, ND_ * sizeof(double));
	memcpy(did_->pre_var, previousVars, NXP_ * sizeof(double));
}

void copyDStatesFromDid(struct DYNInstanceData*did_, double* dStates, double* previousVars){
	if(!did_){
		did_=&tempData;
	}
	memcpy(dStates, did_->discreteState_var, ND_ * sizeof(double));
	memcpy(previousVars, did_->pre_var, NXP_ * sizeof(double));
}

int dsblock_(int* idemand_, int* icall_,
	double* time, double X_[], double XD_[], double U_[],
	double DP_[], int IP_[], Dymola_bool LP_[],
	double F_[], double Y_[], double W_[], double QZ_[],
	double duser_[], int iuser_[], void* cuser_[],
	int* QiErr)
{
	return dsblock_did(idemand_, icall_, time, X_, XD_, U_, DP_, IP_, LP_, F_, Y_, W_, QZ_, duser_, iuser_, cuser_, &tempData, QiErr);
}


void util_print_dymola_timers(fmi2Component c){
	Component* comp = (Component*) c;
	int DymolaTimerStructsLen_=0;
	struct DymolaTimes* DymolaTimerStructs_=0;

	DymolaTimerStructs_=GetDymolaTimers(comp->did, &DymolaTimerStructsLen_);

	if (DymolaTimerStructsLen_>0 && DymolaTimerStructs_) {
	   double overhead=0,overheadSum=0;
	   int i=0;
	   int nrFound=0;
	   for(i=0;i<DymolaTimerStructsLen_;++i){
		   if (DymolaTimerStructs_[i].numAccum>0) {
			   if (nrFound==0 || DymolaTimerStructs_[i].minimAccum<overhead){
				   overhead=DymolaTimerStructs_[i].minimAccum;
			   }
			   nrFound+=DymolaTimerStructs_[i].numAccum;
		   }
	   }
	   if (nrFound>0) {
		  size_t len, maxlen = 14;
		  for (i = 0; i < DymolaTimerStructsLen_; ++i) {
			  if (DymolaTimerStructs_[i].numAccum > 0) {
				  len = strlen(DymolaTimerStructs_[i].name);
				  if (len > maxlen) maxlen = len;
			  }
		  }
		  if (maxlen > 100) maxlen = 100;

		  util_logger(comp, comp->instanceName, fmi2OK, loggUndef,"\nProfiling information for the blocks.\n"
			   "Estimated overhead per call %11.2f[us] total %12.3f[s]\n"
			   "the estimated overhead has been subtracted below.\n"
			   "Name of block%*s, Block, Total CPU[s], Mean[us]    ( Min[us]    to Max[us]    ),   Called\n",
			  overhead*1e6,overhead*nrFound, maxlen - 13, "");
	
		   for(i=0;i<DymolaTimerStructsLen_;++i) if (DymolaTimerStructs_[i].numAccum>0) {
			   double overheadTotal=overhead*DymolaTimerStructs_[i].numAccum;
			   util_logger(comp, "", fmi2OK, loggUndef,"%-*.*s: %5d, %12.3f, %11.2f (%11.2f to %11.2f), %8d\n", maxlen, maxlen,
				   DymolaTimerStructs_[i].name,
				   i,DymolaTimerStructs_[i].totalAccum-overheadTotal,
				   ((DymolaTimerStructs_[i].totalAccum-overheadTotal)/DymolaTimerStructs_[i].numAccum)*1e6,
				   (DymolaTimerStructs_[i].minimAccum-overhead)*1e6,(DymolaTimerStructs_[i].maximAccum-overhead)*1e6,
				   DymolaTimerStructs_[i].numAccum);
		   }
	   }
	}
	return;
}

struct DymolaTimes* GetDymolaTimers(struct DYNInstanceData*did_, int*len) {
	if (!did_) did_=&tempData;
	if (len) *len=did_->DymolaTimerStructsLen_var;
	return did_->DymolaTimerStructs_vec;
}

