#ifndef DYN_INSTANCE_DATA
#define DYN_INSTANCE_DATA

#include"fmi2.h"
#include <stddef.h>
#include "setjmp.h"

#define NGlobalHelp_ 0
#define SizePre_ 1
#define SizeEq_ 2
#define SizeDelay_ 0
#define NWhen_ 0
#define NTim_  0
#define NInitial_ 0
#define NSamp_ 0
#define NCheckIf_ 0
#define NGlobalHelp_ 0
#define NGlobalHelpI_ 0
#define NrDymolaTimers_ 0
#define DymolaNrStepTimers 100000
#define NExternalObject_ 0
#define NI_    0
#define NX_    2
#define MAXAux 0
#define MAXAuxStr_   0
#define MAXAuxStrLen_   500
#define _JBLEN  16
#define DYN_MULTINSTANCE 1
#define DynSimStruct 1


typedef int Dymola_bool;

struct DymolaTimes {
	int num;
	double maxim;
	double minim;
	double total;
	int numAccum;
	double maximAccum;
	double minimAccum;
	double totalAccum;
	const char*name;
	int mask;
};


typedef struct {    /* struct for storing the delay/derivative information */
  double *x;        /* column of time values */
  double *y;        /* column of values, which belong to time values of
		               same row */
  double timedelay; /* value of timedelay (used for delays only) */
  int nx;           /* buffersize of the system = number of columns */
  int first;        /* column of first value in the buffer */
  int old;          /* column of last interpolation step */
  int add;          /* column of last value added to the buffer */
} delayStruct;   


struct ExternalTable_ {
	void* obj_;
	void(*destructor_) (void*);
#if (defined(_OPENMP) && !defined(DISABLE_DYMOLA_OPENMP))
	omp_lock_t lockExternal_;
	int haveLock_;
#endif
};


typedef struct DYNInstanceData {
		  struct BasicDDymosimStruct*basicD;
		  struct BasicIDymosimStruct*basicI;
		  int DymolaOneIteration_var; /* =2 first, =3 subsequent, 4 subsequent no iter, 5 subsequent without iteration, return !=0 => need more */
		  int HaveEventIterated_var;
		  int DymolaEventOptional_var;
		  int inCall_var;
		  delayStruct del_vec[SizeDelay_ ? SizeDelay_ : 1];
		  int delayID_vec[SizeDelay_ ? SizeDelay_ : 1];
		  double helpvar_vec[NGlobalHelp_+1];
		  int helpvari_vec[NGlobalHelpI_+1];
		  double time_var;
		  double InitTime_var;
		  double LastTime_var;
		  double StepSize_var;
		  double currentStepSize_var;
		  double currentStepSizeRatio_var;
		  double currentStepSizeRatio2_var; /* Unused */
		  double previousTime_var;

		  Dymola_bool Event_var;
	      int initializationPhase_var;
	      int Iter_var, MaxIter_var;
          double EPS_var;
          int FirstCross_var;
          Dymola_bool FirstEvent_var;
	      int ResetCounter_var;
		  int HReject_var;

		  int GlobalError_var;
		  int MixedFailFlag_var;
		  Dymola_bool PerformIteration_var;

		  double DymolaHomotopyLambda_var;
		  double DymolaHomotopyLambdaFail_var;
		  double DymolaHomotopyLambdaDelta_var;
		  int DymolaHomotopyUsed_var;
		  int DymolaUserHomotopy_var;

		  double TimeStartOfSimulation_var;

		  double EqRemember1Time_var;
		  double EqRemember2Time_var;
		  struct DymolaTimes DymolaTimerStructs_vec[
                #ifdef NrDymolaTimers_
                			  NrDymolaTimers_ ? NrDymolaTimers_ : 1
                #else
                			  1
                #endif
		  ];
		  int DymolaTimerStructsLen_var;
		  double DymolaStartTimers_vec[
                #ifdef NrDymolaTimers_
                NrDymolaTimers_ ? NrDymolaTimers_ : 1
                #else
                	1
                #endif
			];
          double DymolaTimeZero_vec[DymolaNrStepTimers];
          int DymolaTimeZeroLength_var;
		  int DymolaTimecounter_var;
		  double QPre_vec[SizePre_?SizePre_:1];
		  double RefPre_vec[SizePre_?SizePre_:1];
		  double EqRemember1_vec[SizeEq_?SizeEq_:1];
		  double EqRemember2_vec[SizeEq_?SizeEq_:1];
		  double Aux_vec[MAXAux+10000];
		  Dymola_bool QEvaluate_vec[NWhen_+1];
		  Dymola_bool QEvaluateNew_vec[NWhen_+1]; 
		  double QCheckIf_vec[NCheckIf_+1];
		  Dymola_bool QTimed_vec[NTim_+1];
		  Dymola_bool Qenable_vec[NRel_+1];
		  int oldReset_var;
		  double NextSampleTime_vec[NSamp_+1];
	      double NextSampleTimeNew_vec[NSamp_+1];
	      Dymola_bool NextSampleAct_vec[NSamp_+1];
	      Dymola_bool NextSampleActNew_vec[NSamp_+1];
		  Dymola_bool QL_vec[NRel_+1];
	      double QRel_vec[NRel_+1];
	      double QM_vec[NRel_+1];
	      double Qn_vec[NRel_+1];
	      double Qp_vec[NRel_+1];
	      double Qscaled_vec[NRel_+1];
	      double QZold_vec[2*NRel_+1];
	      double oldQZ_vec[2*NRel_+1];
	      double oldQZDummy_vec[2*NRel_+1];
	      double oldQZ2_vec[2*NRel_+1];
		  double oldQZ3_vec[2*NRel_+1];
		  Dymola_bool Init_var, AnyEvent_var, AnyDEvent_var, AnyREvent_var, AnyIEvent_var, AnyEventParameter_var, NewParameters_var;
		  struct ExternalTable_ externalTable_vec[
                #ifdef NExternalObject_ 
                			NExternalObject_+1
                #else
                			  1
                #endif
		  ];
		  int dymolaParametersNrOld_;
	      int dymolaEventsNr_var;
		  int enforceWhen_var;

	      double * QJacobian_var;
	      double * QBJacobian_var;
	      double * QCJacobian_var;
	      double * QDJacobian_var;
	      int QJacobianN_var;
	      int QJacobianNU_var;
	      int QJacobianNY_var;
	      double * QJacobianSparse_var;
	      int* QJacobianSparseR_var ;
	      int* QJacobianSparseC_var ;
	      int QJacobianNZ_var;
		  int QSparseABCD_var;

		  double QImd_vec[
                #ifdef NI_
                	NI_*(2*NI_+5)+NX_*(4+3*NI_)+	
                #endif
                1
          ];
		  int QIml_vec[
                #ifdef NI_
                	5*NI_+
                #endif
                	1
          ];
		  char DYNAuxStrBuff_vec[
                #if defined(MAXAuxStr_) && MAXAuxStr_>0
                			MAXAuxStr_
                #else
                		  1
                #endif
                		  *
                #if defined(MAXAuxStrLen_) && MAXAuxStrLen_>10
                			MAXAuxStrLen_
                #else
                		  10
                #endif
		  ];
			char* DYNAuxStrPtr_vec[
                #if defined(MAXAuxStr_) && MAXAuxStr_>0
                			MAXAuxStr_
                #else
                		  1
                #endif
			];
			int currentIteration_var;
			double discreteState_var[ND_+1];
			double pre_var[NXP_+1];
			double xInitial_var[NInitial_+1];
			double xInitial_var2[NInitial_+1];
			double decoupleTime_var;
			Dymola_bool sectioncondition_var;
			double Qtol_var;

			double eqRememberAccepted1Time;
			double eqRememberAccepted2Time;
			double EqRememberTemp_vec[SizeEq_ ? SizeEq_ : 1];
			double eqRememberTempTime;
			double eqRememberTempTimeAcc;
			int hasStoredInTemp;
			int eqRememberReplaceOldDynamics;
			int eqRememberReplaceOldAccepted;

			int nonlinearFailureAdvicePrinted;
			char fmiResourceLocation[1024];

			int DymolaHomotopyExponential_var;

			int mixedNiter_var;

			// jmp_buf exception_buffer[MAX_EC];
			int exception_count;
	  } DYNInstanceData;
	  
	  
	  
	  
	  
struct FunctionContext_ {
	char*text;
	struct FunctionContext_*next;
};
	  
struct BufferObject {double *Realbuffer; int *Integerbuffer; int* Sizebuffer; const char* Stringbuffer;};
struct MarkObject {struct BufferObject place,mark;};
	  
struct DYNSpecialMarks {
	/* These ones are "const" */
	const char* m_startstringmark;
	const char* m_endsimplestring;
	double*m_endreal;
	int*m_endinteger;
	int* m_endsize;
	const char* m_endstring;
};

typedef struct __jmp_buf_tag jmp_buf[1];

struct MyJmpBuf {
	jmp_buf TheBuffer;
};

struct DYN_ThreadData {
	struct FunctionContext_*m_global;
	int m_external;
	struct FunctionContext_ m_context;
	struct MarkObject m_start, m_current;
	const char* m_stringmark;
	struct DYNSpecialMarks m_extra;
	struct MyJmpBuf m_jmp_buf_env;
	struct DYNInstanceData* did;
};

struct DYNInstanceDataMinimal {
	struct BasicDDymosimStruct* basicD;
	struct BasicIDymosimStruct* basicI;
};


void DYNInitializeDid(struct DYNInstanceData*did_);

void dymosimSetResources2(struct DYNInstanceData*did_, const char*s);


extern struct DYNInstanceData tempData;
extern Component* globalComponent;
extern Component* globalComponent2;


#endif