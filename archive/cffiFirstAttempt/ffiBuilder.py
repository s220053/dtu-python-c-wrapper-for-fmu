from cffi import FFI

CDEF = '''\
#define NGlobalHelp_ 0
#define SizePre_ 1
#define SizeEq_ 2
#define SizeDelay_ 0
#define NWhen_ 0
#define NTim_  0
#define NInitial_ 0
#define NSamp_ 0
#define NCheckIf_ 0
#define NGlobalHelp_ 0
#define NGlobalHelpI_ 0
#define NrDymolaTimers_ 0
#define DymolaNrStepTimers 100000
#define NExternalObject_ 0
#define NI_    0
#define NX_    2
#define MAXAux 0
#define MAXAuxStr_   0
#define MAXAuxStrLen_   500
#define _JBLEN  16
#define DYN_MULTINSTANCE 1
#define DynSimStruct 1

typedef void*           fmi2ComponentEnvironment;    /* Pointer to FMU environment    */
typedef void*           fmi2FMUstate;                /* Pointer to internal FMU state */
typedef unsigned int    fmi2ValueReference;
typedef double          fmi2Real   ;
typedef int             fmi2Integer;
typedef int             fmi2Boolean;
typedef char            fmi2Char;
typedef const fmi2Char* fmi2String;
typedef char            fmi2Byte;


#define fmi2True  1
#define fmi2False 0

typedef enum {
    fmi2ModelExchange,
    fmi2CoSimulation
} fmi2Type;


typedef enum {
    fmi2OK,
    fmi2Warning,
    fmi2Discard,
    fmi2Error,
    fmi2Fatal,
    fmi2Pending
} fmi2Status;


typedef void      (*fmi2CallbackLogger)        (fmi2ComponentEnvironment componentEnvironment,
                                                fmi2String instanceName,
                                                fmi2Status status,
                                                fmi2String category,
                                                fmi2String message,
                                                ...);
typedef void*     (*fmi2CallbackAllocateMemory)(size_t nobj, size_t size);
typedef void      (*fmi2CallbackFreeMemory)    (void* obj);
typedef void      (*fmi2StepFinished)          (fmi2ComponentEnvironment componentEnvironment,
                                                fmi2Status status);

typedef struct {
   fmi2CallbackLogger         logger;
   fmi2CallbackAllocateMemory allocateMemory;
   fmi2CallbackFreeMemory     freeMemory;
   fmi2StepFinished           stepFinished;
   fmi2ComponentEnvironment   componentEnvironment;
} fmi2CallbackFunctions;


typedef enum {
	modelInstantiated,
	modelInitializationMode, /*inside get macro and therfore required to be deifned for fmi1 */
	modelCsPreEventMode,
	modelEventMode,
	modelEventMode2,    /* event iteration ongoing */
	modelEventModeExit, /* event iteration finished */
	modelContinousTimeMode,
	modelTerminated
} ModelStatus;




typedef struct {
	fmi2Real time;
	fmi2Real* parameters;
	fmi2Real* states;
	fmi2Real* derivatives;
	fmi2Real* outputs;
	fmi2Real* inputs;
	fmi2Real* auxiliary;
} ResultValues;

typedef struct {
	// struct DeclarePhase phase;
	/* last time for sample, possibly buffered */
	fmi2Real lastSampleTime;
	/* last time for sample on file */
	fmi2Real lastFileSampleTime;
	/* last neglected sample warning applies up to this time */
	fmi2Real lastWarnTime;
	/* buffered values */
	ResultValues** buf;
	int curBufSize;
	int curStartIx;
	int curIx;
	int nUsed;
	/* whether have warned about forced flush */
	fmi2Boolean flushWarned;
	unsigned long long resultSampleCounter;
} Result;



typedef struct {
	int nJacA;
	int nJacB;
	int nJacC;
	int nJacD;
	double * jacA;
	double * jacB;
	double * jacC;
	double * jacD;

	size_t nJacV;
	size_t nJacZ;
	double* jacV;
	double* jacVTmp1;
	double* jacVTmp2;
	double* jacZ;
	double* jacZTmp1;
	double* jacZTmp2;

	/*Temporary solution for discreteStates */
	fmi2FMUstate stateStore; 
} JacobianData;



struct BasicIDymosimStruct {
	int mFindEvent;
	int mPrintEvent;
	int mallowSingularFlag;
	int mInJacobian;
	int mSolverHandleEq;
	int mInlineIntegration;
	int mTriggerStepEvent;

	int mDymolaStoreAuxiliaries;

	int mParametersNr;
	int mEvent;
	int mContinueSimulate;

	int mAtDAEEvent;
	int mRememberOnlyAccepted;

	int mTriggerResultSnapshot;

	int mUsingCVodeGodess;

	int mBlockUnblockSmoothCrossings;
};

struct BasicDDymosimStruct {
	double mDymolaFixedStepSize;
	double mCurrentStepSizeRatio2;
	double mNextTimeEvent;
	double mOrigTimeError;
	double mStoreResultInterval;
	double mStartNextTimeEvent;
};


typedef struct tpl_bin {
    void *addr;
    uint32_t sz;
} tpl_bin;


typedef int Dymola_bool;

typedef struct {    /* struct for storing the delay/derivative information */
  double *x;        /* column of time values */
  double *y;        /* column of values, which belong to time values of
		               same row */
  double timedelay; /* value of timedelay (used for delays only) */
  int nx;           /* buffersize of the system = number of columns */
  int first;        /* column of first value in the buffer */
  int old;          /* column of last interpolation step */
  int add;          /* column of last value added to the buffer */
} delayStruct;   


struct DymolaTimes {
	int num;
	double maxim;
	double minim;
	double total;
	int numAccum;
	double maximAccum;
	double minimAccum;
	double totalAccum;
	const char*name;
	int mask;
};

struct ExternalTable_ {
	void* obj_;
	void(*destructor_) (void*);
};

typedef struct DYNInstanceData {
		  struct BasicDDymosimStruct*basicD;
		  struct BasicIDymosimStruct*basicI;
		  int DymolaOneIteration_var; /* =2 first, =3 subsequent, 4 subsequent no iter, 5 subsequent without iteration, return !=0 => need more */
		  int HaveEventIterated_var;
		  int DymolaEventOptional_var;
		  int inCall_var;
		  delayStruct del_vec[1];
		  int delayID_vec[1];
		  double helpvar_vec[1];
		  int helpvari_vec[1];
		  double time_var;
		  double InitTime_var;
		  double LastTime_var;
		  double StepSize_var;
		  double currentStepSize_var;
		  double currentStepSizeRatio_var;
		  double currentStepSizeRatio2_var; /* Unused */
		  double previousTime_var;

		  Dymola_bool Event_var;
	      int initializationPhase_var;
	      int Iter_var, MaxIter_var;
          double EPS_var;
          int FirstCross_var;
          Dymola_bool FirstEvent_var;
	      int ResetCounter_var;
		  int HReject_var;

		  int GlobalError_var;
		  int MixedFailFlag_var;
		  Dymola_bool PerformIteration_var;

		  double DymolaHomotopyLambda_var;
		  double DymolaHomotopyLambdaFail_var;
		  double DymolaHomotopyLambdaDelta_var;
		  int DymolaHomotopyUsed_var;
		  int DymolaUserHomotopy_var;

		  double TimeStartOfSimulation_var;

		  double EqRemember1Time_var;
		  double EqRemember2Time_var;
		  struct DymolaTimes DymolaTimerStructs_vec[1];
		  int DymolaTimerStructsLen_var;
		  double DymolaStartTimers_vec[1];
          double DymolaTimeZero_vec[100000];
          int DymolaTimeZeroLength_var;
		  int DymolaTimecounter_var;
		  double QPre_vec[1];
		  double RefPre_vec[1];
		  double EqRemember1_vec[2];
		  double EqRemember2_vec[2];
		  double Aux_vec[10000];
		  Dymola_bool QEvaluate_vec[1];
		  Dymola_bool QEvaluateNew_vec[1]; 
		  double QCheckIf_vec[1];
		  Dymola_bool QTimed_vec[1];
		  Dymola_bool Qenable_vec[1];
		  int oldReset_var;
		  double NextSampleTime_vec[1];
	      double NextSampleTimeNew_vec[1];
	      Dymola_bool NextSampleAct_vec[1];
	      Dymola_bool NextSampleActNew_vec[1];
		  Dymola_bool QL_vec[1];
	      double QRel_vec[1];
	      double QM_vec[1];
	      double Qn_vec[1];
	      double Qp_vec[1];
	      double Qscaled_vec[1];
	      double QZold_vec[1];
	      double oldQZ_vec[1];
	      double oldQZDummy_vec[1];
	      double oldQZ2_vec[1];
		  double oldQZ3_vec[1];
		  Dymola_bool Init_var, AnyEvent_var, AnyDEvent_var, AnyREvent_var, AnyIEvent_var, AnyEventParameter_var, NewParameters_var;
		  struct ExternalTable_ externalTable_vec[1];
		  int dymolaParametersNrOld_;
	      int dymolaEventsNr_var;
		  int enforceWhen_var;

	      double * QJacobian_var;
	      double * QBJacobian_var;
	      double * QCJacobian_var;
	      double * QDJacobian_var;
	      int QJacobianN_var;
	      int QJacobianNU_var;
	      int QJacobianNY_var;
	      double * QJacobianSparse_var;
	      int* QJacobianSparseR_var ;
	      int* QJacobianSparseC_var ;
	      int QJacobianNZ_var;
		  int QSparseABCD_var;

		  double QImd_vec[9];
		  int QIml_vec[1];
		  char DYNAuxStrBuff_vec[500];
			char* DYNAuxStrPtr_vec[1];
			int currentIteration_var;
			double discreteState_var[1];
			double pre_var[1];
			double xInitial_var[1];
			double xInitial_var2[1];
			double decoupleTime_var;
			Dymola_bool sectioncondition_var;
			double Qtol_var;

			double eqRememberAccepted1Time;
			double eqRememberAccepted2Time;
			double EqRememberTemp_vec[2];
			double eqRememberTempTime;
			double eqRememberTempTimeAcc;
			int hasStoredInTemp;
			int eqRememberReplaceOldDynamics;
			int eqRememberReplaceOldAccepted;

			int nonlinearFailureAdvicePrinted;
			char fmiResourceLocation[1024];

			int DymolaHomotopyExponential_var;

			int mixedNiter_var;

			// jmp_buf exception_buffer[MAX_EC];
			int exception_count;
} DYNInstanceData;
	  

typedef struct {
	fmi2Real tStart;
	fmi2Boolean StopTimeDefined;
	fmi2Real tStop;
	fmi2Boolean relativeToleranceDefined;
    fmi2Real relativeTolerance;
    
	fmi2Boolean isCoSim;

	fmi2String instanceName;
	fmi2Boolean loggingOn;
	fmi2Boolean loggFuncCall;

	size_t nStates;
	size_t nIn;
	size_t nOut;
	size_t nAux;
	size_t nPar;
	size_t nSPar;
	size_t nCross;
	size_t nDStates;
	size_t nPrevious;
	size_t nConstAux;

	fmi2Real* states;
	fmi2Real* parameters;
	fmi2Real* derivatives;
	fmi2Real* inputs;
	fmi2Real* outputs;
	fmi2Real* auxiliary;
	fmi2Real* crossingFunctions;
	fmi2Real* dStates;
	fmi2Real* previousVars;

	fmi2Real* statesNominal;

	fmi2Char** sParameters;
	fmi2Char** tsParameters;

	fmi2Real* oldStates; /*To check if state values have changed durin event*/

	const fmi2CallbackFunctions* functions;
	
	fmi2Real time;
	int icall;

	/* for buffering of short messages without linebreaks */
	char logbuf[256];
	char* logbufp;

	ModelStatus mStatus;
	/* only for FMI 2.0 really, but convenient internally also for 1.0 */
	fmi2Boolean terminationByModel;

	/* for storing of result from within FMU */
	fmi2Boolean storeResult;
	Result* result;

	struct BasicDDymosimStruct* dstruct;
	struct BasicIDymosimStruct* istruct;
	/* to avoid numerous castings */
	double* duser;
	int* iuser;

	/* for internal integration */
	// IntegrationData* iData;

	int allocDone;
	int valWasSet;
	int eventIterRequired;
	int recalJacobian;
	int enforceRefresh; /*Temp due to idemand bug for discret states*/
	int hycosimInputEvent;

	/* for fmiGetDirectionalDerivative */
	JacobianData jacData;
	fmi2Boolean firstEventCall;
	int eventIterationOnGoing;
	void**handles;
	int QiErr;
	struct DYNInstanceData* did;
	unsigned long long inlineStepCounter;
	double nextResultSampleTime;

	fmi2Boolean AdvancedDiscreteStates;
	fmi2Boolean* ClockZeroVector;
	
	tpl_bin* tplDidBin;
	double* serializedDelayData;

	fmi2Boolean anyNonEvent;
	fmi2Boolean steadyStateReached;

	fmi2Boolean fmi2ComputeInit;

	fmi2Boolean maxRunTimeReached;
	double oneSimulationTotalTimerStart;
} Component;

typedef Component* fmi2Component;

fmi2Component fmi2Instantiate(fmi2String instanceName,
						fmi2Type fmuType, 
						fmi2String fmuGUID, 
						fmi2String fmuResourceLocation, 
						const fmi2CallbackFunctions* functions, 
						fmi2Boolean visible, 
						fmi2Boolean loggingOn);



fmi2Status fmi2SetupExperiment(fmi2Component c,
								fmi2Boolean relativeToleranceDefined,
								fmi2Real relativeTolerance,
								fmi2Real tStart,
								fmi2Boolean tStopDefined,
								fmi2Real tStop);
								
								
fmi2Status fmi2EnterInitializationMode(fmi2Component c);

fmi2Status fmi2ExitInitializationMode(fmi2Component c);


fmi2Status fmiTerminate(fmi2Component c);

'''




import os
c_list = [f for f in os.listdir('.') if f.endswith('.c')]    
h_list = ["#include \"" + f + "\"\n" for f in os.listdir('.') if f.endswith('.h')]
SRC = ""
for h in h_list:
    SRC += h


ffibuilder = FFI()
ffibuilder.cdef(CDEF)
ffibuilder.set_source(
    "_fmi2", SRC,
    sources = c_list
)   


if __name__ == "__main__":
    ffibuilder.compile(verbose = True)