#ifndef FMI_INITIALIZE
#define FMI_INITIALIZE


#include "fmi2.h"

#include "dynInstanceData.h"

#define result_setup(c) 0
#define result_sample(c, f) 0
#define DYNCallStackData


fmi2EventInfo eventInfo;


fmi2Status fmi2SetupExperiment(fmi2Component c,
								fmi2Boolean relativeToleranceDefined,
								fmi2Real relativeTolerance,
								fmi2Real tStart,
								fmi2Boolean tStopDefined,
								fmi2Real tStop);

fmi2Status fmi2EnterInitializationMode(fmi2Component c);

fmi2Status fmi2ExitInitializationMode(fmi2Component c);


fmi2Status fmiExitSlaveInitializationMode_(fmi2Component c);

fmi2Status fmiExitModelInitializationMode_(fmi2Component c);

fmi2Status util_exit_model_initialization_mode(fmi2Component c, const char* label, ModelStatus nextStatus);

fmi2Status fmiInitializeSlave(fmi2Component c,
							  fmi2Real tStart,
							  fmi2Boolean StopTimeDefined,
							  fmi2Real tStop);


fmi2Status util_initialize_slave(fmi2Component c,
                                 fmi2Real relativeTolerance,
								 fmi2Real tStart,
								 fmi2Boolean StopTimeDefined,
								 fmi2Real tStop);


fmi2Status util_initialize_model(fmi2Component c,
								 fmi2Boolean toleranceControlled, 
								 fmi2Real relativeTolerance, 
								 fmi2Boolean complete);


void InitializeDymosimStruct(struct BasicDDymosimStruct*basicD,
							 struct BasicIDymosimStruct*basicI);
 
 
void SetDymolaEventOptional(struct DYNInstanceData*did_, int val);

void SetDymolaOneIteration(struct DYNInstanceData*did_, int val);

int GetDymolaOneIteration(struct DYNInstanceData*did_);


int util_refresh_cache(Component* comp, 
					   int idemand, 
					   const char* label, 
					   fmi2Boolean* iterationConverged);
					   
					   


fmi2Status fmiEnterSlaveInitializationMode_(fmi2Component c,
                                            fmi2Real relativeTolerance,
											fmi2Real tStart,
											fmi2Boolean StopTimeDefined,
											fmi2Real tStop);
											
fmi2Status fmiEnterModelInitializationMode_(fmi2Component c, fmi2Boolean toleranceControlled, fmi2Real relativeTolerance);

fmi2Status util_error(Component* comp);


fmi2Status fmiTerminate(fmi2Component c);

fmi2Status fmiTerminateSlave_(fmi2Component c);

fmi2Status fmiTerminateModel_(fmi2Component c);

void copyDStatesToDid(struct DYNInstanceData*did_, double* dStates, double* previousVars);

void copyDStatesFromDid(struct DYNInstanceData*did_, double* dStates, double* previousVars);

int dsblock_(int* idemand_, int* icall_,
	double* time, double X_[], double XD_[], double U_[],
	double DP_[], int IP_[], Dymola_bool LP_[],
	double F_[], double Y_[], double W_[], double QZ_[],
	double duser_[], int iuser_[], void* cuser_[],
	int* QiErr);


int dsblock_did(int *idemand_, int *icall_, 
      double *time, double X_[], double XD_[],  double U_[], 
      double DP_[], int IP_[], Dymola_bool LP_[], 
   double F_[], double Y_[], double W_[], double QZ_[], 
      double duser_[], int iuser_[], void*cuser_[],struct DYNInstanceData* did_, 
      int *QiErr);

void util_print_dymola_timers(fmi2Component c);

struct DymolaTimes* GetDymolaTimers(struct DYNInstanceData*did_, int*len);


typedef double Real;
typedef int Integer;
typedef const char* String;
typedef Integer SizeType;



struct DYNFunctionData_ {
	struct MarkObject*mstartMark;
	struct MarkObject*mcurrentMark;
	int* m_external;
	struct FunctionContext_** m_global;
	struct FunctionContext_* m_context;
	String *m_stringmark;
	struct DYNSpecialMarks m_extra;
	struct MyJmpBuf* m_jmp_buf_env;
	struct ExternalTable_*m_externalTable;
};






#endif