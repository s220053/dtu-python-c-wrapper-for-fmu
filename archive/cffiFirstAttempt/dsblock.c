#include "fmiInitialize.h"
#include "fmi2.h"
#include "dynInstanceData.h"
#include <setjmp.h>

// static __thread struct DYN_ThreadData dyn_threadData = { 0 };

// static struct DYN_ThreadData* DYN_GetThreadData() {
// 	return &dyn_threadData;
// }

// void DYNPropagateDidToThread(struct DYNInstanceData* did) {
// 	DYN_GetThreadData()->did = did;
// }

// struct DYNFunctionData_ DYNGetFunctionData() {
// 	struct DYNFunctionData_ dynFuncData;
// #if defined(DYN_MULTINSTANCE)
// 	struct DYN_ThreadData* threadData = 0;
// 	EnsureMarkInitialized();
// 	threadData = DYN_GetThreadData();
// 	dynFuncData.mstartMark = &threadData->m_start;
// 	dynFuncData.mcurrentMark = &threadData->m_current;
// 	dynFuncData.m_stringmark = &threadData->m_stringmark;
// 	dynFuncData.m_external = &threadData->m_external;
// 	dynFuncData.m_global = &threadData->m_global;
// 	dynFuncData.m_context = &threadData->m_context;
// 	dynFuncData.m_extra = threadData->m_extra;
// 	dynFuncData.m_jmp_buf_env = &threadData->m_jmp_buf_env;
// 	dynFuncData.m_externalTable = DymosimGetExternalObject2(threadData->did);
// 	if (0 == dynFuncData.m_externalTable) dynFuncData.m_externalTable = DymosimGetExternalObject();
// #else
// 	EnsureMarkInitialized();
// 	dynFuncData.mstartMark = &startMark;
// 	dynFuncData.mcurrentMark = &currentMark;
// 	dynFuncData.m_stringmark = &stringmark;
// 	dynFuncData.m_external = &FunctionExternalContext_;
// 	dynFuncData.m_context = &modelContext_;
// 	dynFuncData.m_global = &FunctionContextGlobal_;
// 	dynFuncData.m_extra.m_startstringmark = startstringmark;
// 	dynFuncData.m_extra.m_endsimplestring = Endsimplestring;
// 	dynFuncData.m_extra.m_endreal = EndRealbuffer;
// 	dynFuncData.m_extra.m_endinteger = EndIntegerbuffer;
// 	dynFuncData.m_extra.m_endsize = EndSizebuffer;
// 	dynFuncData.m_extra.m_endstring = EndStringbuffer;
// 	dynFuncData.m_jmp_buf_env = &DYNGlobal_jmp_buf_env;
// 	dynFuncData.m_externalTable = DymosimGetExternalObject();
// #endif
// 	return dynFuncData;
// }

int dsblock_did(int *idemand_, int *icall_, 
      double *time, double X_[], double XD_[],  double U_[], 
      double DP_[], int IP_[], Dymola_bool LP_[], 
   double F_[], double Y_[], double W_[], double QZ_[], 
      double duser_[], int iuser_[], void*cuser_[],struct DYNInstanceData* did_, 
      int *QiErr)
{
	return 0;

// #ifdef DYNCallStackData
// 	struct DYNFunctionData_ dynFuncData, *DYNStackData_;
// #endif
// #if defined(DS_EMBEDDED)
// 	dse_SetStatePointers(sts, idemand_, icall_, time, X_, XD_, U_, DP_, IP_, LP_, F_, Y_, W_, QZ_, did_ ? did_->Aux_vec : 0, duser_, iuser_, cuser_, QiErr);
// #endif

// 	DYNPropagateDidToThread(did_);
// #ifdef DYNCallStackData
// 	/* After propagate */
// 	dynFuncData = DYNGetFunctionData();
// 	DYNStackData_ = &dynFuncData;
// #endif
//         if (did_->inCall_var && !*idemand_ == 0) {if (*idemand_ != 3) *QiErr=1; return 0;} /* Recursive call, allow in special case */ 
// 	did_->inCall_var = 1;
      
// 	  if (setjmp(DYNPCSDGlobal_jmp_buf.TheBuffer) == 0)  {

//       DYNTime2 = *time;
//       Init_ = *idemand_ == 0;
// 	  did_->HaveEventIterated_var = false;
// 	  if (DymolaOneIteration_==0 || DymolaOneIteration_==2) {AnyDEvent_=false;AnyIEvent_=false;AnyREvent_=false;}
// 	  if (Init_ && (DymolaOneIteration_==0 || DymolaOneIteration_==2) ) {
// 		  did_->dymolaParametersNrOld_=0;
// 		  dymolaParametersNr_++;
// 	  }
// 	  NewParameters_ = did_->dymolaParametersNrOld_!=dymolaParametersNr_;
// 	  if (NewParameters_) *icall_=0;
// 	  did_->dymolaParametersNrOld_=dymolaParametersNr_;
// 	  if (Init_ && ((struct BasicIDymosimStruct*)iuser_)->mContinueSimulate) dymolaParametersNr_++; /* To trigger it again */
		
//       DYNEvent = Init_  || *idemand_ == 5 || *idemand_ == 7 ||
//         (! RootFinder_ && *idemand_ == 3) || NewParameters_;
// 	  (((struct BasicIDymosimStruct*)iuser_)->mEvent)=DYNEvent;
// 	  if (Init_) {
// 		  ++ResetCounter_;
// 		  GlobalError_ = 0;
// 	  }
// 	  GlobalError_ = 0;
// /*    GlobalError_ = 0; */  /* In order to handle errors at events. */ 

// 	  rememberOnlyAccepted_ = 0;
//       if (Init_) {
// 		  {int i__; for (i__ = 0; i__ < NrDymolaTimers_; ++i__) {
// 			  did_->DymolaTimerStructs_vec[i__].mask = 0;
// 			  did_->DymolaTimerStructs_vec[i__].name = "";
// 		  }}
// 		  DYNAdvicePrinted_ = 0;
// 		  Dymola_Simulation_TriggerResultSnapshot_ = 0;
// #if defined(DYNHideWindowsPopup) && defined(_WIN32)
// 		  SetErrorMode(SEM_NOGPFAULTERRORBOX);
// #endif
// #if defined(GenerateSimulinkTiming) && defined(DynSimStruct) && !defined(DYMOLA_DSPACE)
// 	    did_->TimeStartOfSimulation_var=CurrentClockTime;
// #endif
// 		EqRemember1Time_=-1e33;
// 	    EqRemember2Time_=-1e33;
// 		EqRemAcc1Time_ = -1e33;
// 		EqRemAcc2Time_ = -1e33;
// 		EqRemTempTime_ = -1e33;
// 		EqRemTempTimeAcc_ = -1e33;
// 		(did_->hasStoredInTemp) = 0;
// 		(did_->eqRememberReplaceOldDynamics) = 0;
// 		(did_->eqRememberReplaceOldAccepted) = 0;
// 		(did_->decoupleTime_var)=-1e33;
// 		InitTime=DYNTime;
//         FirstEvent = true;
// #if defined(PrintEvent_)
//         PrintEvent = PrintEvent_;
// #else
//         PrintEvent = 0;
// #endif
// #ifdef DYNExponentialHomotopyLambda_
// 		(did_->DymolaHomotopyExponential_var) = 1;
// #else
// 		(did_->DymolaHomotopyExponential_var) = 0;
// #endif
//         MaxIter = 25;
//         EPS_ = 1e-10;

// 		DYNMixedNiter_ = 0;

// 		{int i1_;
// #ifdef DYMOSIM
// 		 int Ierror_;
//         i1_ = 2;
// #if !defined(PrintEvent_)
//         blosii_(&i1_, &PrintEvent, &Ierror_);
// #endif

//         i1_ = 3;
//         blosii_(&i1_, &MaxIter, &Ierror_);

//         i1_ = 4;
//         blosid_(&i1_, &EPS_, &Ierror_);

//         i1_ = 5;
//         blosid_(&i1_, &Qtol, &Ierror_);
// #else
// #ifdef TOLERANCE_FROM_SETUP
// 		Qtol = TOLERANCE_FROM_SETUP;
// #else
//         Qtol = 1.E-4;
// #endif /* TOLERANCE_FROM_SETUP */
// #endif

//         dymmdp_();
// 		if (!DelayStruct_[0].x) {
// 			if (allocDelayBuffers(DelayStruct_, SizeDelay_ ? SizeDelay_ : 1, Buffersize)) {
// 				*QiErr = -500;
// 				did_->inCall_var = 0;
// 				return 0;
// 			}
// 		}
//         for (i1_ = 0; i1_ < QNLmax; i1_++) {
//           QNLfunc[i1_] = 0;
//           QNLjac[i1_] = 0;
// 		  QNLcalls[i1_] = 0;
//         }
// 		}

//         FirstCross_ = 1;
//         GetDimensions2(&nx_, &nx2_, &nu_, &ny_, &nw_, &np_, &nsp_, &nrel2_, &nrel_, &ncons_, &dae_);
//       }
//       if (DYNEvent && !did_->DymolaEventOptional_var) {
//            triggerStepEvent_=0;
//       }
// /* End dsblock2.c */


// /* Begin dsblock3.c
//    File version: 1.3, 1998-10-29 */
//    /*
// 	* Copyright (C) 1997-2001 Dynasim AB.
// 	* All rights reserved.
// 	*
// 	*/

// 	  if (Init_) {
// 		  int i1_;
// 		  for (i1_ = 1; i1_ <= nrel_; i1_++) {
// 			  QZold_[2 * i1_ - 2] = 1;
// 			  QZold_[2 * i1_ - 1] = 1;
// 			  QZ_[2 * i1_ - 2] = 1;
// 			  QZ_[2 * i1_ - 1] = 1;
// 		  }
// 	  }

// 	  if (Init_) {
// 		  int i1_;
// 		  for (i1_ = 0; i1_ < sizeof(QPre_) / sizeof(QPre_[0]); i1_++) {
// 			  RefPre_[i1_] = 0;
// 			  QPre_[i1_] = 0;
// 		  }
// 	  }

// 	  if (blockUnblockSmoothCrossings_) {
// 		  int i1_;
// 		  if (blockUnblockSmoothCrossings_ == 1) {
// 			  for (i1_ = 1; i1_ <= nrel_; i1_++) {
// 				  if (QL_[i1_] & 128)
// 					  QL_[i1_] |= 256;
// 			  }
// 		  }
// 		  else if (blockUnblockSmoothCrossings_ == 2) {
// 			  const int mask = 127;
// 			  for (i1_ = 1; i1_ <= nrel_; i1_++)
// 				  QL_[i1_] &= mask;
// 		  }
// 		  blockUnblockSmoothCrossings_ = 0;
// 	  }

// 	  if (DymolaOneIteration_ < 3) Iter = 0;
// 	  if (did_->DymolaEventOptional_var && *idemand_ == 5 && RootFinder_) {
// 		  if (DYNTime < NextTimeEvent && !triggerStepEvent_) {
// 			  DYNEvent = false;
// 			  *idemand_ = 4;
// 			  Iter = 1;
// 		  }
// 	  }
// 	  if (DymolaOneIteration_ == 5) goto iterate2;
//   iterateEvent:
// 	  if (DYNEvent) {
// 		  NextTimeEvent = 1e100;
// 		  did_->HaveEventIterated_var = true;
// 	  }
// 	  if (*idemand_ == 4 || (DYNEvent && DymolaOneIteration_ != 3 && DymolaOneIteration_ != 4)) {
// 		  int i1_;
// 		  for (i1_ = 1; i1_ <= nrel_; i1_++) Qenable_[i1_] = false;
// 	  }
// 	  if (DYNEvent) {
// 		  ClearNextSample(did_);
// 	  }
//   iterate:
// 	  DYNHReject = 0;
// 	  if (DYNEvent) ++dymolaEventsNr_;
// 	  if ((Iter % 11) == 10) {
// 		  int i1_;
// 		  for (i1_ = 1; i1_ <= nrel_; i1_++) Qenable_[i1_] = false;
// 	  }
// 	  PerformIteration_ = false;
// 	  Iter = Iter + 1;
// 	  if ((!(*idemand_ == 0) && PrintEvent & (1 << 1)) || ((*idemand_ == 0) && PrintEvent & (1 << 2))) DynLogEvents(*idemand_ == 0 && !Init_ ? Iter + 1 : Iter, 0, 0, 0, 0, 0);
// 	  AnyEvent_ = false;
// 	  AnyEventParameter_ = false;
// #if defined(DYNCALLFMUREINIT)
// 	  DelayedUpdateReinit(time, X_, W_, U_, Y_, did_);
// #endif
//   iterate2:
// 	  equations_(idemand_, icall_,
// 		  time, X_, XD_, U_,
// 		  DP_, IP_, LP_,
// 		  F_, Y_, W_, QZ_,
// 		  duser_, iuser_, cuser_, did_ DYNOPTCallCSD,
// 		  QiErr);
// 	  if (*QiErr >= 1 && *QiErr <= 4 && DYNTime <= InitTime && Iter <= 1 && DymolaHomotopyUsed && !DymolaUserHomotopy) {
// 		  if (ResetCounter_ != did_->oldReset_var) {
// 			  did_->oldReset_var = ResetCounter_;
// 			  if (!(PrintEvent & (1 << 10))) DymosimMessage("Trying to solve non-linear system using global homotopy-method.");
// 			  DymolaUserHomotopy = 1;
// 			  *QiErr = -997;
// 		  }
// 	  }
// 	  if (*QiErr == -997) {
// 		  if (DymolaUserHomotopy && DymolaHomotopyLambda != 0) {
// 			  DymolaHomotopyLambda = 0;
// 			  UpdateInitVars(time, X_, XD_, U_, DP_, IP_, LP_,
// 				  F_, Y_, W_, QZ_, duser_, iuser_, cuser_, did_, 0);
// 			  GlobalError_ = 0;
// 			  *QiErr = 0;
// 			  goto iterate2;
// 		  }
// 	  }
// 	  else if (*QiErr == -996) {
// 		  if (DymolaUserHomotopy) {
// 			  GlobalError_ = 0;
// 			  *QiErr = 0;
// 			  if (DymolaHomotopyLambda < 1) {
// 				  if (DymolaHomotopyLambdaDelta<0.08 && DymolaHomotopyLambda>DymolaHomotopyLambdaFail + DymolaHomotopyLambdaDelta * 6)
// 					  DymolaHomotopyLambdaDelta *= 2;
// 				  DymolaHomotopyLambda += DymolaHomotopyLambdaDelta;
// 				  if (DymolaHomotopyLambda > 1 - 1e-9) DymolaHomotopyLambda = 1;
// 				  DYNHReject = 0;
// 				  UpdateInitVars(time, X_, XD_, U_, DP_, IP_, LP_,
// 					  F_, Y_, W_, QZ_, duser_, iuser_, cuser_, did_, 0);
// 				  goto iterate2;
// 			  }
// 			  else DymolaUserHomotopy = 0;
// 		  }
// 	  }
// 	  else if (*QiErr != 0 && DymolaUserHomotopy) {
// 		  if (DymolaHomotopyLambda <= 0) DymosimMessage("Error: could not solve simplified initialization for homotopy method.");
// 		  else if (DymolaHomotopyLambdaDelta <= 1e-4) {
// 			  if (DymolaHomotopyLambdaFail > 0)
// 				  DymosimMessage("Error: adaptive homotopy method got stuck after starting. Can set scripting flag Advanced.DebugHomotopy=true; simulate again, and open continuation.csv to investigate");
// 			  else
// 				  DymosimMessage("Error: adaptive homotopy method could not start using actual - check that actual and simplified arguments are similar.");
// 		  }
// 		  else {
// 			  GlobalError_ = 0;
// 			  *QiErr = 0;
// 			  DymolaHomotopyLambda -= DymolaHomotopyLambdaDelta;
// 			  DymolaHomotopyLambdaDelta *= 0.5;
// 			  DymolaHomotopyLambdaFail = DymolaHomotopyLambda;
// 			  DymolaHomotopyLambda += DymolaHomotopyLambdaDelta;
// 			  DymosimMessage("Error: reducing step size for homotopy.");
// 			  DYNHReject = 1;
// 			  UpdateInitVars(time, X_, XD_, U_, DP_, IP_, LP_,
// 				  F_, Y_, W_, QZ_, duser_, iuser_, cuser_, did_, 0);
// 			  goto iterate2;
// 		  }
// 	  }

// 	  if (PerformIteration_)
// 		  goto iterate;

// 	  if (DYNEvent) {

// 		  /* End dsblock3.c */

// 		  /* Begin file dsblock4.c */
// /* File version: 1.7, 1999-01-20 */

// /*
//  * Copyright (C) 1997-2001 Dynasim AB.
//  * All rights reserved.
//  *
//  */

//  /* end */
// 	  }
// 	  if (DymolaOneIteration_ == -1) DymolaOneIteration_ = 0;
// 	  if (did_->DymolaEventOptional_var && *idemand_ == 4 && Iter == 2 && RootFinder_) {
// 		  int i1_;
// 		  for (i1_ = 0; i1_ < nrel_ * 2; i1_++) {
// 			  if (QZold_[i1_] * QZ_[i1_] < 0)
// 				  AnyEvent_ = true;
// 		  }
// 		  if (AnyEvent_) {
// 			  *idemand_ = 5;
// 			  DYNEvent = true;
// 			  Iter = 0;
// 			  goto iterateEvent;
// 		  }
// 	  }
// 	  if (AnyEvent_)
// 		  did_->HaveEventIterated_var = true;

// 	  if (initializationPhase_ || (((Init_ && nrel_ > 0) || AnyEvent_) && (FirstEvent || EventIterate_) && Iter <= MaxIter)) {
// 		  if (GlobalError_ != 0 || *QiErr != 0) goto leave;
// 		  if ((!(*idemand_ == 0) && PrintEvent & (1 << 1)) || ((*idemand_ == 0) && PrintEvent & (1 << 2))) {
// 			  if (DymolaOneIteration_ == 0 || DymolaOneIteration_ == 4)
// 				  DymosimMessage("Iterating to find consistent restart conditions.");
// 		  }
// 		  else if (Iter == MaxIter) {
// 			  if (!(PrintEvent & (1 << 10))) {
// 				  DymosimMessage("");
// 				  DymosimMessage("On the final iteration for restart conditions we get:");
// 				  PrintEvent |= (1 << 1);
// 			  }
// 		  }
// 		  if (initializationPhase_ == 1)
// 			  initializationPhase_ = 2;
// 		  else {
// 			  if (Init_) Iter = 0;
// 			  Init_ = false;
// 			  initializationPhase_ = 0;
// 		  }
// 		  NewParameters_ = NewParameters_ && AnyEventParameter_;
// 		  DYNEvent = true;
// 		  *icall_ = 0;
// 		  if (DymolaOneIteration_ == 4) {
// 			  DymolaOneIteration_ = 0;
// 		  }
// 		  else if (DymolaOneIteration_) {
// 			  DymolaOneIteration_ = 3;
// 			  goto leave;
// 		  }
// 		  goto iterate;
// 	  }
// 	  else if (Iter > MaxIter) {
// 		  DymosimMessage("");
// 		  DymosimMessageDouble("ERROR: Finding consistent restart conditions failed at time: ", DYNTime);
// 		  DymosimMessage("");
// 		  if ((!(*idemand_ == 0) && PrintEvent & (1 << 1)) || ((*idemand_ == 0) && PrintEvent & (1 << 2)))
// 			  DynLogEvents(-5, DYNTime, *idemand_ == 0 ? 1 : (*idemand_ == 7 ? 2 : 0), 0, 0, 0);
// 		  *QiErr = 1;
// 		  GlobalError_ = 1;
// 		  goto leave;
// 	  }
// 	  if (DymolaOneIteration_ != 0) {
// 		  int i;
// 		  for (i = 0; i < NWhen_; ++i) { QEvaluate_[i] = QEvaluateNew_[i]; };
// 	  }
// 	  DymolaOneIteration_ = 0;
// 	  Init_ = FirstEvent;      /* restore Init */
// 	  FirstEvent = false;

// 	  {
// 		  if (((Init_ && nrel_ > 0) || AnyEvent_ || Iter > 1) && ((!(*idemand_ == 0) && PrintEvent & (1 << 1)) || ((*idemand_ == 0) && PrintEvent & (1 << 2)))) {
// 			  DymosimMessageDouble("      during event at Time : ", DYNTime);
// 		  }
// 		  if ((*idemand_ == 0) && (PrintEvent & (1 << 1) || PrintEvent & (1 << 2))) DynLogEvents(-3, DYNTime, 0, 0, (PrintEvent & (1 << 2)), 0);
// 		  if (*idemand_ == 7 && PrintEvent & (1 << 1)) DynLogEvents(-4, DYNTime, 0, 0, 0, 0);
// 	  }
// #ifdef DYNEventSpecial
// 	  if (*QiErr == 0 && !Init_ && DYNEvent && !AnyDEvent_ && !AnyREvent_ && (*idemand_ == 5 || *idemand_ == 7)) {
// 		  if (AnyIEvent_) {
// 			  *QiErr = -994;
// 		  }
// 		  else {
// #ifdef DYNHasMultiRate_
// 			  * QiErr = -993;
// #else
// 			  * QiErr = -995;
// #endif
// 			  DynLogEvents(-6, 0.0, 0, NULL, 0, 0.0);
// 			  if (*idemand_ == 5 && did_->hasStoredInTemp && DYNInitialGuessPolynomialUpdate_) {
// 				  unsigned ii_;
// 				  EqRemember2Time_ = EqRemTempTime_; EqRemAcc2Time_ = EqRemTempTimeAcc_;
// 				  for (ii_ = 0; ii_ < SizeEq_; ii_++) EqRemember2_[ii_] = EqRememberTemp_[ii_];
// 			  }
// 		  }
// 	  }
// #endif
// 	  if (*idemand_ <= 2 || *idemand_ >= 5) did_->hasStoredInTemp = 0;

// 	  if (DYNFindEvent) CheckForEvents(did_, DYNTime, Init_, DYNEvent, QZ_, nrel2_, F_, (DYNFindEvent == 1) ? (nx_) : (0), duser_, iuser_);

// #if 1
// 	  if (DAEsolver_ && (DYNEvent || atDAEEvent_)) {
// 		  int i_;
// 		  for (i_ = 0; i_ < NX_; i_++)
// 			  XD_[i_] = F_[i_];
// 		  if (DYNEvent)
// 			  atDAEEvent_ = 0;
// 	  }
// #endif

// 	  /* Modify only enabled crossing functions. */
// 	  if (SpuriousEvents_ && *idemand_ == 4) {
// 		  int i1_;
// 		  for (i1_ = 1; i1_ <= nrel_; i1_++) {
// 			  if (FirstCross_ || Qenable_[i1_]) {
// 				  QZold_[2 * i1_ - 2] = QZ_[2 * i1_ - 2];
// 				  QZold_[2 * i1_ - 1] = QZ_[2 * i1_ - 1];
// 			  }
// 			  else {
// 				  QZ_[2 * i1_ - 2] = QZold_[2 * i1_ - 2];
// 				  QZ_[2 * i1_ - 1] = QZold_[2 * i1_ - 1];
// 			  }
// 		  }
// 		  FirstCross_ = 0;
// 	  }
// 	  else if (!SpuriousEvents_ && DYNEvent) {
// 		  int i1_;
// 		  const int mask1 = ~(126);
// 		  const int mask2 = ~(124);
// 		  for (i1_ = 1; i1_ <= nrel_; i1_++)
// 			  if (!Qenable_[i1_]) {
// 				  if (QL_[i1_] & 2 || QZ_[2 * i1_ - 1] == 0) {
// 					  QZ_[2 * i1_ - 2] = QZ_[2 * i1_ - 1] = 0.1;
// 				  }
// 				  QL_[i1_] &= mask1;
// 			  }
// 			  else {
// 				  QL_[i1_] &= mask2;
// 			  }
// 		  if (did_->DymolaEventOptional_var) for (i1_ = 0; i1_ < 2 * nrel_; i1_++) { QZold_[i1_] = QZ_[i1_]; }
// 	  }


// }
//  else {   /* return jump from setjmp/longjmp */
// 	 *QiErr = 1L;
// 	 if (DYNTime <= InitTime && Iter <= 1 && DymolaHomotopyUsed) {
// 		 /* similar as in dsblock3.c - but in case the evaluation failed */
// 		 if (DymolaUserHomotopy) {
// 			 if (DymolaHomotopyLambda <= 0) DymosimMessage("Error: could not solve simplified initialization for homotopy method.");
// 			 else if (DymolaHomotopyLambdaDelta <= 1e-4) {
// 				 if (DymolaHomotopyLambdaFail > 0)
// 					 DymosimMessage("Error: adaptive homotopy method got stuck after starting. Can set scripting flag Advanced.DebugHomotopy=true; simulate again, and open continuation.csv to investigate");
// 				 else
// 					 DymosimMessage("Error: adaptive homotopy method could not start using actual - check that actual and simplified arguments are similar.");
// 			 }
// 			 else {
// 				 GlobalError_ = 0;
// 				 *QiErr = 0;
// 				 DymolaHomotopyLambda -= DymolaHomotopyLambdaDelta;
// 				 DymolaHomotopyLambdaDelta *= 0.5;
// 				 DymolaHomotopyLambdaFail = DymolaHomotopyLambda;
// 				 DymolaHomotopyLambda += DymolaHomotopyLambdaDelta;
// 				 DymosimMessage("Error: reducing step size for homotopy.");
// 				 DYNHReject = 1;
// 				 UpdateInitVars(time, X_, XD_, U_, DP_, IP_, LP_,
// 					 F_, Y_, W_, QZ_, duser_, iuser_, cuser_, did_, 0);
// 				 goto iterate2;
// 			 }
// 		 }
// 		 else if (ResetCounter_ != did_->oldReset_var) {
// 			 did_->oldReset_var = ResetCounter_;
// 			 if (!(PrintEvent & (1 << 10))) DymosimMessage("Trying to solve non-linear system using global homotopy-method.");
// 			 DymolaUserHomotopy = 1;
// 			 DymolaHomotopyLambda = 0;
// 			 UpdateInitVars(time, X_, XD_, U_, DP_, IP_, LP_,
// 				 F_, Y_, W_, QZ_, duser_, iuser_, cuser_, did_, 0);
// 			 GlobalError_ = 0;
// 			 *QiErr = 0;
// 			 goto iterate2;
// 		 }
// 	 }
// #ifdef AutoResetAfter
// 	 if (*QiErr != 0 && *QiErr != -999 && *QiErr != -998) {
// 		 resetActive = 1;
// 		 restartTime = DYNTime + AutoResetAfter;
// 		 *QiErr = GlobalError_ = 0;
// 	 }
// #endif
// 	 did_->inCall_var = 0;
// 	 return 0;
// }

// #if defined(Sections_) 
// *icall_ = *idemand_;
// #else
// * icall_ = 4;
// #endif

// leave:
// #if !defined(DYMOLA_DSPACE) && !defined(NO_FILE) && defined(DymolaPrecisionTiming_) && defined(DymosimRealTimePriority_)
// if (*idemand_ == 7) {
// 	int i, maxi = 0;
// 	for (i = 0;; ++i) {
// 		if (i * 2 >= did_->DymolaTimeZeroLength_var || did_->DymolaTimeZero_vec[2 * i + 1] == 0)
// 			break;
// 	}
// 	maxi = i;
// 	if (maxi > 0) {
// 		FILE* f = fopen("plotTiming.mos", "w");
// 		fprintf(f, "times=fill(0.1,%d,2);\n", maxi);
// 		for (i = 0; i < maxi; ++i) {
// 			fprintf(f, "times[%d,:]={%g,%g};\n", i + 1, did_->DymolaTimeZero_vec[2 * i], did_->DymolaTimeZero_vec[2 * i + 1]);
// 		}
// 		fclose(f);
// 		DymosimMessage("RunScript(\"plotTiming.mos\",true);plotArray(times[:,1],times[:,2],-1);");
// 	}
// }
// #endif
// #if defined(GenerateSimulinkTiming) && defined(DynSimStruct) && !defined(DYMOLA_DSPACE)
// if (*idemand_ == 7) {
// 	DymosimMessageDouble("Time for solution:", CurrentClockTime - did_->TimeStartOfSimulation_var);
// }
// #endif
// if (GlobalError_ != 0 && *QiErr == 0)
// *QiErr = GlobalError_;
// #ifdef AutoResetAfter
// if (*QiErr != 0 && *QiErr != -999 && *QiErr != -998) {
// 	resetActive = 1;
// 	restartTime = DYNTime + AutoResetAfter;
// 	*QiErr = GlobalError_ = 0;
// }
// #endif
// did_->inCall_var = 0;
// return 0;


}