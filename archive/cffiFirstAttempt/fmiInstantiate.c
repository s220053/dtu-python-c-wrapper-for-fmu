#include "fmi2.h"
#include "dynInstanceData.h"
#include <stdio.h>

const char* GUIDString="{17f62517-d3fa-44e2-9eae-e2218a143339}";

const char* loggFuncCall = "FunctionCall";
const char* loggUndef = "";
const char* loggQiErr = "";
const char* loggBadCall ="IllegalFunctionCall";
const char* loggSundials ="Sundials";
const char* loggMemErr = "memoryAllocation";
const char* loggStats = "CvodeStatistics";

unsigned int FMIClockValueReferences_[1]={0};

size_t dyn_allowMultipleInstances = sizeof(struct DYNInstanceData);
size_t dyn_instanceDataSize = sizeof(struct DYNInstanceData);
Component* globalComponent = NULL;
Component* globalComponent2 = NULL;

size_t DYNGetMaxAuxStrLen() {
	  return
#if defined(MAXAuxStrLen_) && MAXAuxStrLen_>10
      MAXAuxStrLen_
#else
	  10
#endif
      ;
}


void GetDimensions4(int *nx_, int *nx2_, int *nu_, int *ny_, int *nw_, int *np_, int* nsp_,
int*nrel2_,int *nrel_, int *ncons_, int *dae_, int *nd_, int* nxp_, int* nwp_){
	  *nx_ = NX_;
      *nx2_ = NX2_; 
      *nu_ = NU_;
      *ny_ = NY_;
      *nw_ = NW_;
	  *nsp_ = NPS_;
      *np_ = NP_;
      *nrel_ = NRel_;
	  *nrel2_ = NRelF_;
      *ncons_ = NCons_;
      *dae_ = DAEsolver_;
	  *nd_ = ND_;
	  *nxp_ = NXP_;
	  *nwp_ = NWP_;
}

FMIString util_strdup(FMIString s)
{
	static const FMIString nullString = "<NULL>";
	static const int maxLen = 1024;
	FMIString pos = s;
	int len;
	char* newS;

	if (s == NULL) {
		s = nullString;
	}
	len = (int) strlen(s);
	if (len > maxLen) {
		len = maxLen;
	}
	
	newS = (FMIChar*) ALLOCATE_MEMORY(len + 1, sizeof(FMIChar));
	if (newS == NULL) {
		return NULL;
	}
	strncpy(newS, s, len);
	return newS;
}

int fmiUser_Instantiate() {
	#ifdef FMI_USER_INSTANTIATE
		return FMI_USER_INSTANTIATE();
	#else
		return 0;
	#endif
}

fmi2Component fmi2Instantiate(fmi2String instanceName,
						fmi2Type fmuType, 
						fmi2String fmuGUID, 
						fmi2String fmuResourceLocation, 
						const fmi2CallbackFunctions* functions, 
						fmi2Boolean visible, 
						fmi2Boolean loggingOn)
{
	fmi2Component comp = fmiInstantiate_(instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
	return comp;
}


fmi2Component fmiInstantiate_(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID, fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions, fmi2Boolean visible, fmi2Boolean loggingOn){

	fmi2Component comp;
	if(fmuType==fmi2CoSimulation){
		comp = (fmi2Component) fmiInstantiateSlave_(instanceName, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
		if(comp)
			comp->isCoSim = fmi2True;
	}else if(fmuType==fmi2ModelExchange){
		comp = (fmi2Component) fmiInstantiateModel_(instanceName, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
		if(comp)
			comp->isCoSim = fmi2False;
	}else{
		return NULL;
	}
	return comp;
}



FMIComponent fmiInstantiateSlave_(FMIString instanceName,
												FMIString fmuGUID,
												FMIString fmuResourceLocation,
												const FMICallbackFunctions* functions,
												FMIBoolean visible,
												FMIBoolean loggingOn)
{
	static FMIString label = "fmi2Instantiate";
	Component* comp = (Component*) fmiInstantiateModel_(instanceName, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
	/* ignore argument "visible" since we never provide any GUI */
	if (comp == NULL) {
		return NULL;
	}

	// if (functions->stepFinished != NULL) {
	// 	util_logger(comp, comp->instanceName, FMIWarning, loggBadCall,
	// 		"%s: Callback function stepFinished != NULL but asynchronous fmiDoStep is not supported",label);
	// }
	// util_logger(comp, comp->instanceName, FMIOK, loggFuncCall, "%s", label);
	return comp;
}




FMIComponent fmiInstantiateModel_(FMIString instanceName,
								  FMIString fmuGUID,
								  FMIString fmuResourceLocation,
								  const FMICallbackFunctions* functions,
								  FMIBoolean visible,
								  FMIBoolean loggingOn)
{
	static FMIString label = "fmi2Instantiate";
	Component* comp;
	size_t i = 0;
	int QiErr = 0;
	const size_t maxStringSize = DYNGetMaxAuxStrLen();
	const int fmiClockedStates = 0;
	const FMICallbackFunctions* funcs = functions;
	JacobianData* jacData;
	
	if (!dyn_allowMultipleInstances && globalComponent != NULL) return NULL;
	
	if(fmiUser_Instantiate()) return NULL;
	
	comp = (Component*) ALLOCATE_MEMORY(1 , sizeof(Component));
	if (comp == NULL) {
		goto fail;
	}	
	if (dyn_allowMultipleInstances) {
		comp->did = (struct DYNInstanceData*)ALLOCATE_MEMORY(1, dyn_allowMultipleInstances);
		if (comp->did == NULL) {
			goto fail;
		}
		DYNInitializeDid(comp->did);
	} else {
		comp->did = 0;
	}
	
	char offset = 0;
	if (fmuResourceLocation != 0 && strncmp(fmuResourceLocation, "file:/", 6) == 0){
			offset += 5;
		if (fmuResourceLocation != 0 && strncmp(fmuResourceLocation + 6, "//", 2) == 0){
			/* handle case file:/// */
			offset += 2;	
		}
	}
	
	/*in Fmi 3 the path is no longer required to start with file:/, 0 offset is OK*/
	if(offset){ 
		dymosimSetResources2(comp->did, fmuResourceLocation + offset);
		/* Note comp->did may be null, that is handled. */
	}
	
	comp->handles=0;
	/* Initialize to NULL to facilitate freeing om memory */
	comp->dstruct = NULL;
	comp->istruct = NULL;
	comp->states = comp->derivatives = comp->parameters = comp->inputs = comp->outputs = comp->auxiliary =
		comp->crossingFunctions = comp->statesNominal = NULL;

	comp->isCoSim = FMIFalse; /*Default, change later if Co-Sim*/
	/* set sensible default start time */
	comp->time = 0;
	comp->logbufp = comp->logbuf;
	comp->functions = funcs;
	comp->instanceName = util_strdup(instanceName);
	if (comp->instanceName == NULL) {
		goto fail;
	}
	comp->loggingOn = loggingOn;

	/* verify GUID */
	if (strcmp(fmuGUID, GUIDString) != 0) {
		// util_logger(comp, instanceName, FMIError, loggBadCall, "Invalid GUID: %s, expected %s", fmuGUID, GUIDString);
		goto fail;
	}

	comp->maxRunTimeReached = FMIFalse;
	comp->oneSimulationTotalTimerStart = 0.0;

	comp->dstruct = (struct BasicDDymosimStruct*) ALLOCATE_MEMORY(1, sizeof(struct BasicDDymosimStruct));
	comp->istruct = (struct BasicIDymosimStruct*) ALLOCATE_MEMORY(1, sizeof(struct BasicIDymosimStruct));
	if (comp->dstruct == NULL || comp->istruct == NULL) {
		goto fail;
	}
	comp->duser = (double*) comp->dstruct;
	comp->iuser = (int*) comp->istruct;
	if (comp->did) {
		(( struct DYNInstanceDataMinimal*)comp->did)->basicD=comp->dstruct;
		(( struct DYNInstanceDataMinimal*)comp->did)->basicI=comp->istruct;
	}

	// setBasicStruct((double*) comp->dstruct, (int*) comp->istruct);

	int nx, nx2, nu, ny, nw, np, nsp, nrel2, nrel, ncons, dae, nd, nxp, nwp;
	GetDimensions4(&nx, &nx2, &nu, &ny, &nw, &np, &nsp, &nrel2, &nrel, &ncons, &dae, &nd, &nxp, &nwp);
	comp->nStates = nx;
	comp->nIn = nu;
	comp->nOut = ny;
	comp->nAux = nw;
	comp->nPar = np;
	comp->nSPar = nsp;
	comp->nCross = 2 * nrel;
	comp->nDStates = nd;
	comp->nPrevious = nxp;
	comp->nConstAux = nwp;

	/* Guard against zero value for size by adding one */
	comp->states = (FMIReal*) ALLOCATE_MEMORY(comp->nStates + 1, sizeof(FMIReal));
	comp->derivatives = (FMIReal*) ALLOCATE_MEMORY(comp->nStates + 1, sizeof(FMIReal));
	comp->parameters = (FMIReal*) ALLOCATE_MEMORY(comp->nPar + 1, sizeof(FMIReal));
	comp->inputs = (FMIReal*) ALLOCATE_MEMORY(comp->nIn + 1, sizeof(FMIReal));
	comp->outputs = (FMIReal*) ALLOCATE_MEMORY(comp->nOut + 1, sizeof(FMIReal));
	comp->auxiliary = (FMIReal*) ALLOCATE_MEMORY(comp->nAux + 1, sizeof(FMIReal));
	comp->crossingFunctions = (FMIReal*) ALLOCATE_MEMORY(comp->nCross + 1, sizeof(FMIReal));
	comp->statesNominal = (FMIReal*) ALLOCATE_MEMORY(comp->nStates + 1, sizeof(FMIReal));
	comp->sParameters = (FMIChar**) ALLOCATE_MEMORY(comp->nSPar + 1, sizeof(FMIChar*));
	comp->oldStates = (FMIReal*) ALLOCATE_MEMORY(comp->nStates + 1, sizeof(FMIReal));
	comp->dStates = (FMIReal*) ALLOCATE_MEMORY(comp->nDStates + 1, sizeof(FMIReal));
	comp->previousVars = (FMIReal*) ALLOCATE_MEMORY(comp->nPrevious + 1, sizeof(FMIReal));
	comp->ClockZeroVector = (FMIBoolean*) ALLOCATE_MEMORY(FMIClockValueReferences_[0]+1, sizeof(FMIBoolean));

	if (comp->states == NULL || comp->derivatives == NULL || comp->parameters == NULL ||
		comp->inputs == NULL || comp->outputs == NULL || comp->auxiliary == NULL ||
		comp->crossingFunctions == NULL || comp->statesNominal == NULL || comp->sParameters == NULL) {
			goto fail;
	}
	/*Temporary fmiString pointers to retreve  original fmiStrings when calling reset
	 allocated if needed in reset*/
	comp->tsParameters = NULL; 
	/* default values */
	for (i = 0; i < comp->nStates; i++) {
		comp->statesNominal[i] = 1.0;
	}

	comp->mStatus = modelInstantiated;
	comp->storeResult = FMIFalse;
	comp->enforceRefresh = FMIFalse;
	// comp->iData = NULL;

	jacData = &comp->jacData;
	jacData->jacA = jacData->jacB = jacData->jacC = jacData->jacD = NULL;
	jacData->jacV = jacData->jacVTmp1 = jacData->jacVTmp2 = jacData->jacZ = jacData->jacZTmp1 = jacData->jacZTmp2 = NULL;
	jacData->nJacA = jacData->nJacB = jacData->nJacC = jacData->nJacD = 0;
	jacData->nJacV = jacData->nJacZ = 0;
	jacData->stateStore = 0;
	comp->recalJacobian = 1;

	/* FMI API does not require caller to set start values, so must fetch start values for
	   states and parameters (other variables are initiated by dsblock_) */
	// declareNew2_(comp->states, comp->derivatives, comp->parameters, comp->inputs, comp->outputs, comp->auxiliary, comp->statesNominal, (void**) comp->sParameters, &QiErr, 0, 0);
	
	for(i = 0; i < comp->nStates; ++i){
		if(comp->statesNominal[i] <= 0.0)
			comp->statesNominal[i] = 1.0;
	}
	for(i=0; i < comp->nSPar; ++i){
		FMIString s=(comp->sParameters)[i];
		size_t len;
		len=strlen(s);
		if (len>maxStringSize) len=maxStringSize;
		(comp->sParameters)[i] = (FMIChar*) ALLOCATE_MEMORY(maxStringSize+1, sizeof(FMIChar));
		memcpy((comp->sParameters)[i], s, len+1);
		(comp->sParameters)[i][maxStringSize]='\0';
	}

	if (QiErr != 0) {
		// util_logger(comp, comp->instanceName, FMIError, loggQiErr,
			// "%s: declare_ failed, QiErr = %d", label, QiErr);
		goto fail;
	}	
	/* Default values for setup experiment */
	comp->tStart = 0;
	comp->StopTimeDefined = FMIFalse;
	comp->tStop = 0;
	comp->relativeToleranceDefined = FMIFalse;
	comp->relativeTolerance = 0;

	comp->valWasSet = 0;
	comp->eventIterRequired = 0;

	comp->AdvancedDiscreteStates = fmiClockedStates;
	comp->hycosimInputEvent = 0;
	comp->nextResultSampleTime = 1e100;
	comp->fmi2ComputeInit = 0;
	
	if (!dyn_allowMultipleInstances) globalComponent = comp;
	
	comp->tplDidBin = (tpl_bin*)ALLOCATE_MEMORY(1, sizeof(tpl_bin));
	if (comp->tplDidBin == NULL) {
		goto fail;
	}
	
	// util_logger(comp, comp->instanceName, FMIOK, loggFuncCall, "%s completed", label, QiErr);
	
	// printf("%s instantiation succeeded", comp->instanceName);
	
	return comp;

fail:
	printf("%s instantiation failed", comp->instanceName);
	if (comp != NULL) {
		FMIString iName = instanceName != NULL ? instanceName : "";
		// util_logger(comp, iName, FMIFatal, loggUndef, "Instantiation failed");
		// fmiFreeModelInstance_(comp);
	}
	return NULL;
}