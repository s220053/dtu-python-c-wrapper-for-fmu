import ctypes
import model_description


# define python/c interface type
fmi2Component = ctypes.c_void_p
fmi2ComponentEnvironment = ctypes.c_void_p
fmi2FMUstate = ctypes.c_void_p
fmi2ValueReference = ctypes.c_uint
fmi2Real = ctypes.c_double
fmi2Integer = ctypes.c_int
fmi2Boolean  = ctypes.c_int
fmi2Char = ctypes.c_char
fmi2String = ctypes.c_char_p
fmi2Type = ctypes.c_int
fmi2Byte = ctypes.c_char
fmi2Status = ctypes.c_int

fmi2CallbackLoggerTYPE = ctypes.CFUNCTYPE(None, fmi2Component, fmi2String, fmi2Status, fmi2String, fmi2String)
fmi2CallbackAllocateMemoryTYPE = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_size_t, ctypes.c_size_t)
fmi2CallbackFreeMemoryTYPE = ctypes.CFUNCTYPE(None, ctypes.c_void_p)



# load dll
twoMasses = ctypes.CDLL("../TwoMasses/binaries/win64/TwoMasses.dll")



# call dll functions
typesPlatform = getattr(twoMasses,"fmi2GetTypesPlatform")
typesPlatform.restype = ctypes.c_char_p
version = getattr(twoMasses,"fmi2GetVersion")
version.restype = ctypes.c_char_p
print("platform:", typesPlatform().decode(), "\nversion:", version().decode())



# define callback function
class fmi2CallbackFunctions(ctypes.Structure):
    """
    typedef struct { 
         void (*logger)(fmiComponent c, fmiString instanceName, fmiStatus status, 
         fmiString category, fmiString message, ...); 
         void* (*allocateMemory)(size_t nobj, size_t size); 
         void (*freeMemory) (void* obj); 
    } fmiCallbackFunctions;
    """
    _fields_ = [("logger", fmi2CallbackLoggerTYPE),
                ("allocateMemory", fmi2CallbackAllocateMemoryTYPE), 
                ("freeMemory", fmi2CallbackFreeMemoryTYPE)]
                
def testPrint():
    print("print log")

callbackFunction = fmi2CallbackFunctions()
callbackFunction.logger = fmi2CallbackLoggerTYPE(testPrint)




# read model description
modelDescription = model_description.read_model_description("../TwoMasses/modelDescription.xml")
print("model name: ", modelDescription.modelName,"\nGUID: " , modelDescription.guid)




# Instantiate
Instantiate = getattr(twoMasses,"fmi2Instantiate")
# Instantiate.argtypes = [fmi2String, fmi2String, fmi2CallbackFunctions,fmi2Boolean]
Instantiate(modelDescription.modelName,modelDescription.guid,callbackFunction, True)
print("FMU model instantiated")
