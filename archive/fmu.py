import gym

from pkg_resources import resource_exists, resource_filename, resource_listdir

#from fmutest import euler_step, FMU_ME

from cool_data_rl.utils.formatting import printf

import shutil

import numpy as np

#added to check FMU

from fmpy import extract, read_model_description, instantiate_fmu, simulate_fmu

 

class FMUWrapper(gym.Wrapper):

    def __init__(self, env, fmu, fmu_stepsize = None, **kwargs):

        super().__init__(env)

        # self.env = env

 

        if isinstance(fmu, str):

            printf("WARN: When passing the FMU as a str the FMU will be initialized for each created environment. This is not recommended as it can take up a lot of space and slow computation time.", color = "yellow")

            self.fmu_file = fmu

            fmu_stepsize = fmu_stepsize if fmu_stepsize is not None else 900

            #added to initilize FMU once only

            # extract the FMU to a temporary directory

            self.unzipdir = extract(fmu)

            # read the model description

            self.model_description = read_model_description(self.unzipdir)

            # instantiate the FMU

            self.fmu = instantiate_fmu(self.unzipdir, self.model_description, 'ModelExchange')

            #self.fmu = FMU_ME(fmu=resource_filename("gym_cool_data", "data/FMU/" + self.fmu_file), step_size=fmu_stepsize)

        else:

            self.fmu = fmu

 

        # update server temperature function

        self.env.__setattr__("get_next_server_temperature", self.get_next_server_temperature)

        self.unwrapped.__setattr__("get_next_server_temperature", self.get_next_server_temperature)

 

    def get_next_server_temperature(self, cooling: dict):

        '''

        Calculate new server temperature after applying the given cooling to the state

 

        The new server temperature is calculated using the given FMU.

        '''

        # print("Using new")

 

        total_cooling = sum([cooling[system] for system in self.COOLING_SYSTEMS])

 

        start_values={'cooling_demand': self.data.cooling_load/self.data.observation_hours,

            'total_cooling': total_cooling/self.data.observation_hours,

            'server_temp_in': 273.15 + self.server_temperature,

            'SERVER_ROOM_TEMP': 273.15 + self.ROOM_TEMP}

        

        #new_temp = euler_step(fmu = self.fmu, inputs=input)

        next = simulate_fmu(self.unzipdir, start_values=start_values, start_time=0, stop_time=900, model_description=self.model_description, fmu_instance=self.fmu)

        new_temp = next['server_temp_out']

        server_temperature = new_temp[-1]-273.15

        #server_temperature = new_temp[1]-273.15

        #print("FMU temp: ", server_temperature)

        return server_temperature

 

    def close(self) -> None:

        super().close()

        self.fmu.cleanup()

 

    #added to reset FMU at each env step before taking an action and simulation of fmu

    def step(self, action):

        self.fmu.reset()

        return super().step(action)

    

    def reset(self, seed = None) -> np.ndarray:     

        obs = super().reset()

        # obs = self.env.reset(seed=seed)

        

        try:

            #self.fmu.cleanup()

            # self.fmu.freeInstance()

            #shutil.rmtree(self.fmu.uzd, ignore_errors=True)

            # self.fmu.terminate()

            #reset the fmu after each environment reset call

            self.fmu.reset()

 

        except:

            pass

            

            # self.fmu = FMU_ME(fmu=resource_filename("gym_cool_data", self.fmu_file), step_size=self.fmu_stepsize)

        return obs

 

 

 

 

if __name__ == "__main__":

    import cool_data_rl.envs

    env = gym.make("NaviairEnv-v0", episode_len = 35, verbose = False)

    env2 = gym.make("NaviairEnv-v0", episode_len = 35, verbose = False)

 

    fmu_files = {

        "windows": [

            "WP4_Naviair.Components.C0R0_FMU.fmu",

            "WP4_Naviair.Components.Continuous.C1R1_FMU.fmu",

            "WP4_Naviair.Components.Continuous.C2R2_FMU.fmu",

            "WP4.Components.Continuous.ATES_QSetPointPID.fmu",

        ],

        "linux": [

            "WP4.Components.BaseClasses.C0R0_kW_Linux.fmu",

            "WP4.Components.Continuous.C1R1_kW_Linux.fmu",

            "WP4_Components_Continuous_C1R1_ME_kW.fmu",

            "WP4_Components_Continuous_C1R1_0kW_MECS.fmu"

        ],

    }

    fmu_file = 3

    fmu_file = resource_filename("cool_data_rl", "data/naviair/FMU/linux/" + fmu_files["linux"][fmu_file])

 

    #fmu = FMU_ME(fmu=fmu_file, step_size=1)

    fmu = fmu_file

    env = FMUWrapper(env, fmu)

 

    env.reset()

    env2.reset()

    from datetime import datetime

    init_time = datetime.now()

    print("Init FMU temp: ", env.state["server_temperature"], "\tInit normal temp: ", env2.state["server_temperature"])

    done = False

    n_steps = 20000

    print('Number of iterations: ', n_steps)

    print('*'*30, 'Check only FMU single environment: ', '*'*30)

 

    for i in range(n_steps):        

        action = env.action_space.sample()

        observation, reward, done, info = env.step(action)

        if (done):

            env.reset()

 

        #observation, reward, done, info = env2.step(action)

        #if (done):

            #env2.reset()

        # print(observation)

        #print("FMU temp: ", env.state["server_temperature"], "\tNormal temp: ", env2.state["server_temperature"])

    #check the execution time

    fin_time = datetime.now()

    print ("Execution time: ", (fin_time-init_time))

    # print(env.data.observation_hours)