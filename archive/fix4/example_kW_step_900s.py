from fmutest import euler_step, FMU_ME
from datetime import datetime

class State:

    def __init__(self, initial_temp = 30, initial_demand = 200):
        self.server_temp = initial_temp
        self.cooling_demand = initial_demand


class CoolingSystem:

    def __init__(self, **kwargs):
        self.SYSTEMS = ["ATES", "FC", "Chiller"]
        self.SERVER_ROOM_TEMP = kwargs.get("server_room_temp", 25)


    def _next_server_temp(self, fmu, state, cooling: dict):

        total_cooling = sum([cooling[system] for system in self.SYSTEMS])
        print(total_cooling)
        next = euler_step(fmu = fmu, 
                          inputs={'cooling_demand': state.cooling_demand,
                                  'total_cooling': total_cooling,
                                  'server_temp_in': state.server_temp, 
                                  'SERVER_ROOM_TEMP': self.SERVER_ROOM_TEMP})
        return next[1]


if __name__=='__main__':

    cooling = {"ATES": 50, "FC": 10, "Chiller": 30}
    state = State()
    cs = CoolingSystem()
    #print(state.server_temp)

    #print("Execution time: ", (fin_time-init_time))
    inputs={'cooling_demand': state.cooling_demand,
            'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
            'server_temp_in': state.server_temp,
            'SERVER_ROOM_TEMP': 25}

    fmu = FMU_ME(fmu='WP4.Components.BaseClasses.C0R0_kW.fmu', step_size=900)
    fmu1 = FMU_ME(fmu='WP4.Components.Continuous.C1R1_kW.fmu', step_size=900)

    init_time = datetime.now()
    server_temp = 30;
    heat_capacity = 80;
    cooling_demand = 200;
    alpha = 0.5;


    for i in range(33):
        cooling.update({"ATES": 50*(1+i), "FC": 10*(1+i), "Chiller": 30*(1+i)})
        #print(cooling)
        inputs.update({'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
                       'server_temp_in': server_temp})
        total_cooling = 50*(1+i) + 10*(1+i) + 30*(1+i);
        server_temp = server_temp + 1 / heat_capacity * (cooling_demand - total_cooling + alpha * (25 - server_temp));

        result = euler_step(fmu, inputs)
        result1 = euler_step(fmu1, inputs)

        state.server_temp = result[1]

        print(state.server_temp)
        print(server_temp)
        print(result1[1])
        print((server_temp-result1[1])/server_temp)

    fin_time = datetime.now()

    print("Execution time: ", (fin_time-init_time))
    print(state.server_temp)