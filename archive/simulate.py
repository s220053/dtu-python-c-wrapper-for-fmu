import importlib
from fmi2Functions import Fmi2Functions
from model_description import read_model_description
from datetime import datetime
import gc
import tracemalloc





class State:

    def __init__(self, initial_temp = 30, initial_demand = 200):
        self.server_temp = initial_temp
        self.cooling_demand = initial_demand




class CoolingSystem:

    def __init__(self, **kwargs):
        self.SYSTEMS = ["ATES", "FC", "Chiller"]
        self.SERVER_ROOM_TEMP = kwargs.get("server_room_temp", 25)




def  _setInput(comp, inputs, functions, modelDescription):
    module = importlib.import_module(modelDescription.modelName)
    for iName, iValue in inputs.items():
        for variable in modelDescription.modelVariables:
            if variable.name == iName:
                v = module.ffi.new("fmi2Real*")
                v[0] = iValue
                vr = module.ffi.new("const fmi2ValueReference*")
                vr[0] = variable.valueReference
                functions.fmi2SetReal(comp, vr, 1, v)




def _getResult(modelDescription, comp, functions):
    module = importlib.import_module(modelDescription.modelName)
    
    outputs = []
    for variable in modelDescription.modelVariables:
        if variable.causality == 'output':            
            outputs.append(variable)
            
    result = []
    for output in outputs:
        if output.type == "Real":
            v = module.ffi.new("fmi2Real*")
            vr = module.ffi.new("const fmi2ValueReference*")
            vr[0] = output.valueReference
            functions.fmi2GetReal(comp, vr, 1, v)
            result.append(v[0])

    return result





def setUp(functions, path, modelDescription, startTime, stopTime,**kwargs):
    module = importlib.import_module(modelDescription.modelName)

    # instantiate
    comp = functions.fmi2Instantiate(module.ffi.new("char[]", modelDescription.modelName.encode()), # instanceName
                                     module.lib.fmi2CoSimulation, # fmuType
                                     module.ffi.new("char[]", modelDescription.guid.encode()), # fmuGUID
                                     module.ffi.new("char[]", path.encode()), # fmuResourceLocation
                                     module.ffi.new('fmi2CallbackFunctions *'), # functions
                                     True, # visible
                                     False) # loggingOn      
    
    # initialize
    functions.fmi2SetupExperiment(comp,
                                  True, # toleranceDefined
                                  0.0, # tolerance
                                  startTime, # startTime
                                  True, # stopTimeDefined
                                  stopTime) # stopTime
    functions.fmi2EnterInitializationMode(comp)
    status = functions.fmi2ExitInitializationMode(comp)
    
    if status == 0:
        return comp
    else:
        raise ValueError("setUp failed")
    
    
    
    
    
    
def doStep(comp, inputs, functions, modelDescription, time, outputInterval):

    _setInput(comp, inputs, functions, modelDescription)

    status = functions.fmi2DoStep(comp,
                                  time, # currentCommunicationPoint
                                  outputInterval, # communicationStepSize
                                  False) # noSetFMUStatePriorToCurrentPoint        
            
    if status == 0:
        return _getResult(modelDescription, comp, functions)
    else:
        raise ValueError("doStep failed")
        





if __name__ == "__main__":

    cooling = {"ATES": 50, "FC": 10, "Chiller": 30}
    state = State()
    cs = CoolingSystem()

    inputs={'cooling_demand': state.cooling_demand,
            'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
            'server_temp_in': state.server_temp,
            'SERVER_ROOM_TEMP': 25}

    modelDescription1 = read_model_description("../../resource/codegen_fmus/WP4_Components_BaseClasses_C0R0_0kW/modelDescription.xml")
    modelDescription1.modelName = "WP4_Components_BaseClasses_C0R0_0kW"
    functions1 = Fmi2Functions(modelDescription1.modelName)
    fmu1 = setUp(functions = functions1, 
                 path = "../../resource/codegen_fmus/WP4_Components_BaseClasses_C0R0_0kW.fmu", 
                 modelDescription = modelDescription1,
                 startTime = 0, 
                 stopTime = 900)
    
    modelDescription2 = read_model_description("../../resource/codegen_fmus/WP4_Components_Continuous_C1R1_0kW/modelDescription.xml")
    modelDescription2.modelName = "WP4_Components_Continuous_C1R1_0kW"
    functions2 = Fmi2Functions(modelDescription2.modelName)
    fmu2 = setUp(functions = functions2, 
                 path = "../../resource/codegen_fmus/WP4_Components_Continuous_C1R1_0kW.fmu", 
                 modelDescription = modelDescription2,
                 startTime = 0, 
                 stopTime = 900)
    
    server_temp = 30
    heat_capacity = 80
    cooling_demand = 200
    alpha = 0.5
    
    init_time = datetime.now()
    
    # tracemalloc.start()  # Start tracing memory allocations
    
    for i in range(10):

        cooling.update({"ATES": 50*(1+i), "FC": 10*(1+i), "Chiller": 30*(1+i)})
        inputs.update({'total_cooling': sum([cooling[system] for system in ["ATES", "FC", "Chiller"]]),
                       'server_temp_in': server_temp})
        total_cooling = 50*(1+i) + 10*(1+i) + 30*(1+i)
        server_temp = server_temp + 1 / heat_capacity /900 * (cooling_demand - total_cooling + alpha * (25 - server_temp))
        
        # snapshot_before = tracemalloc.take_snapshot()  # Take a snapshot before the suspected leak

        outputInterval = 0.0001
        print(i*outputInterval, outputInterval)
        result1 = doStep(fmu1, inputs, functions1, modelDescription1, i * outputInterval, outputInterval)
        result2 = doStep(fmu2, inputs, functions2, modelDescription2, i * outputInterval, outputInterval)
        
        # snapshot_after = tracemalloc.take_snapshot()  # Take a snapshot after the suspected leak
        # top_stats = snapshot_after.compare_to(snapshot_before, 'lineno')
        # for stat in top_stats[:5]:
        #     print(stat)

        
        state.server_temp = result1[0]
        if i>99990:
            print(result1[0])
            print(result2[0])
        
        # print(state.server_temp)
        # print(server_temp)
        # print(result2[0])
        # print((server_temp - result2[0])/server_temp)


    fin_time = datetime.now()
    
    print("Execution time: ", (fin_time - init_time))
    print(state.server_temp)
    
    

