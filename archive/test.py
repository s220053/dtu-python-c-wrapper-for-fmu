from _fmi2 import lib, ffi
from model_description import read_model_description


# convert C string to Python string
to_python_string = lambda c_string: ffi.string(c_string).decode()


# parse model description file
path = "../../TwoMasses/"
modelDescription = read_model_description(path+"modelDescription.xml")
v = modelDescription.modelVariables





# instantiate
comp = lib.TwoMasses_fmi2Instantiate(ffi.new("char[]", modelDescription.modelName.encode()), # instanceName
                                     lib.fmi2CoSimulation, # fmuType
                                     ffi.new("char[]", modelDescription.guid.encode()), # fmuGUID
                                     ffi.new("char[]", path.encode()), # fmuResourceLocation
                                     ffi.new('fmi2CallbackFunctions *'), # functions
                                     True, # visible
                                     False) # loggingOn        
if comp is None:
    pass



# initialize
tStart, tStop = 0, 200000
lib.TwoMasses_fmi2SetupExperiment(comp,
                                  True, # toleranceDefined
                                  0.0, # tolerance
                                  tStart, # startTime
                                  True, # stopTimeDefined
                                  tStop) # stopTime
lib.TwoMasses_fmi2EnterInitializationMode(comp)
status = lib.TwoMasses_fmi2ExitInitializationMode(comp)


# configure value
RealReference = [r.valueReference for r in v if
                        r.type == "Real" and 
                        (r.causality=="input" or 
                        (r.causality=="parameter" and r.variability=="tunable")) ]
# RealReference = ffi.new('fmi2ValueReference[]', RealReference)
RealValue = ffi.new('fmi2Real[]', [0 for i in range(len(RealReference))])

IntegerReference = [r.valueReference for r in v if
                        r.type == "Integer" and 
                        (r.causality=="input" or 
                        (r.causality=="parameter" and r.variability=="tunable")) ]
# IntegerReference = ffi.new('fmi2ValueReference[]', IntegerReference)
IntegerValue = ffi.new('fmi2Integer[]', [0 for i in range(len(IntegerReference))])

BooleanReference = [r.valueReference for r in v if
                        r.type == "Boolean" and 
                        (r.causality=="input" or 
                        (r.causality=="parameter" and r.variability=="tunable")) ]
# BooleanReference = ffi.new('fmi2ValueReference[]', BooleanReference)
BooleanValue = ffi.new('fmi2Boolean[]', [0 for i in range(len(BooleanReference))])

StringReference = [r.valueReference for r in v if
                        r.type == "String" and 
                        (r.causality=="input" or 
                        (r.causality=="parameter" and r.variability=="tunable")) ]
# StringReference = ffi.new('fmi2ValueReference[]', StringReference)
StringValue = ffi.new('fmi2String[]', [" " for i in range(len(StringReference))])




# simluate
while tStart < tStop:
    status = lib.TwoMasses_fmi2DoStep(comp,
                                      tStart, # currentCommunicationPoint
                                      1, # communicationStepSize
                                      False) # noSetFMUStatePriorToCurrentPoint
    lib.TwoMasses_fmi2GetReal(comp, 
                              RealReference, 
                              len(RealReference), 
                              RealValue)
    lib.TwoMasses_fmi2SetReal(comp, 
                              RealReference, 
                              len(RealReference), 
                              RealValue)
    
    lib.TwoMasses_fmi2GetInteger( comp, 
                                  IntegerReference, 
                                  len(IntegerReference), 
                                  IntegerValue)
    lib.TwoMasses_fmi2SetInteger( comp, 
                                  IntegerReference, 
                                  len(IntegerReference), 
                                  IntegerValue)
                          
    lib.TwoMasses_fmi2GetBoolean( comp, 
                                  BooleanReference, 
                                  len(BooleanReference), 
                                  BooleanValue)
    lib.TwoMasses_fmi2SetBoolean( comp, 
                                  BooleanReference, 
                                  len(BooleanReference), 
                                  BooleanValue)
    
    lib.TwoMasses_fmi2GetString( comp, 
                                 StringReference, 
                                 len(StringReference), 
                                 StringValue)
    lib.TwoMasses_fmi2SetString( comp, 
                                 StringReference, 
                                 len(StringReference), 
                                 StringValue)
                          
    if status > 0:
        break
    tStart += 1
    

# terminate
if status != 3 and status != 4: 
    lib.TwoMasses_fmi2Terminate(comp)
if status != 4:
    lib.TwoMasses_fmi2FreeInstance(comp)

