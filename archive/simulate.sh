#!/bin/bash

# Function to display usage information
usage() {
    echo "Usage: $0 [-n name] [-h]"
    echo "  -n name  Model Identifier / Model Name -n"
    echo "  -h        Display this help message"
    exit 1
}

# Initialize variables with default values
arg_n=""

# Process command-line arguments
while getopts "n:p:h" opt; do
    case "$opt" in
    n) arg_n="$OPTARG" ;;
    h) usage ;;
    *) usage ;;
    esac
done

# Check if -h was used or if required arguments are missing
if [ "$arg_n" = "" ]; then
    usage
fi



# actual command
logFile="./log/$arg_n.log" 
python3 main.py -n $arg_n 2> $logFile
