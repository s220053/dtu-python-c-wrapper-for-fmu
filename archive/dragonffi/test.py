import pydffi
import os
import sys
sys.path.append("./sources")

# path = "."
# c_list = [path+f for f in os.listdir(path) if f.endswith('.c')]
# c_list = ["#include \"" + f + "\"\n" for f in os.listdir(path) if f.endswith('.c')]
# h_list = ["#include \"" + f + "\"\n" for f in os.listdir(path) if f.endswith('.h')]
# HD = ""
# SRC = ""
# for c in c_list:
#     SRC += c
# for h in h_list:
#     HD += h


list = ["#include \"./sources/" + f + "\"\n" for f in os.listdir('./sources')]
SRC = ''
for f in list:
    SRC += f
print(SRC)

F = pydffi.FFI()
CU = F.compile(SRC)

# CU = F.cdef("#include <stdio.h>"