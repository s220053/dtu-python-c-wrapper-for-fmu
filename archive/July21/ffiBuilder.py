from cffi import FFI


CDEF = '''\

    typedef void*           fmi2Component;               /* Pointer to FMU instance       */
    typedef void*           fmi2ComponentEnvironment;    /* Pointer to FMU environment    */
    typedef void*           fmi2FMUstate;                /* Pointer to internal FMU state */
    typedef unsigned int    fmi2ValueReference;
    typedef double          fmi2Real   ;
    typedef int             fmi2Integer;
    typedef int             fmi2Boolean;
    typedef char            fmi2Char;
    typedef const fmi2Char* fmi2String;
    typedef char            fmi2Byte;
    
    typedef enum {
	    fmi2OK,
	    fmi2Warning,
	    fmi2Discard,
	    fmi2Error,
	    fmi2Fatal,
	    fmi2Pending
	} fmi2Status;
	
	typedef enum {
	    fmi2ModelExchange,
	    fmi2CoSimulation
	} fmi2Type;
	
	typedef void (*fmi2CallbackLogger) (fmi2ComponentEnvironment componentEnvironment,
                                        fmi2String instanceName,
                                        fmi2Status status,
                                        fmi2String category,
                                        fmi2String message,
                                        ...);
	typedef void* (*fmi2CallbackAllocateMemory) (size_t nobj, size_t size);
	typedef void (*fmi2CallbackFreeMemory) (void* obj);
	typedef void (*fmi2StepFinished) (fmi2ComponentEnvironment componentEnvironment,
                                      fmi2Status status);
	
	
	typedef struct {
		fmi2CallbackLogger         logger;
		fmi2CallbackAllocateMemory allocateMemory;
		fmi2CallbackFreeMemory     freeMemory;
		fmi2StepFinished           stepFinished;
		fmi2ComponentEnvironment   componentEnvironment;
	} fmi2CallbackFunctions;

	/*
    fmi2Component fmi2Instantiate(fmi2String instanceName,
						fmi2Type fmuType, 
						fmi2String fmuGUID, 
						fmi2String fmuResourceLocation, 
						const fmi2CallbackFunctions* functions, 
						fmi2Boolean visible, 
						fmi2Boolean loggingOn);
	*/					
						
						
						
						
						
						
						
    typedef void*           FMIComponent;               /* Pointer to FMU instance       */
    typedef void*           fmiComponentEnvironment;    /* Pointer to FMU environment    */
    typedef void*           fmiFMUstate;                /* Pointer to internal FMU state */
    typedef unsigned int    fmiValueReference;
    typedef double          fmiReal   ;
    typedef int             fmiInteger;
    typedef int             FMIBoolean;
    typedef char            fmiChar;
    typedef const fmiChar* FMIString;
    typedef char            fmiByte;	
    typedef enum {
	    fmiOK,
	    fmiWarning,
	    fmiDiscard,
	    fmiError,
	    fmiFatal,
	    fmiPending
	} fmiStatus;
    
	typedef void (*fmiCallbackLogger) (fmi2ComponentEnvironment componentEnvironment,
                                        fmi2String instanceName,
                                        fmi2Status status,
                                        fmi2String category,
                                        fmi2String message,
                                        ...);
	typedef void* (*fmiCallbackAllocateMemory) (size_t nobj, size_t size);
	typedef void (*fmiCallbackFreeMemory) (void* obj);
	
    typedef struct {
     fmiCallbackLogger         logger;
     fmiCallbackAllocateMemory allocateMemory;
     fmiCallbackFreeMemory     freeMemory;
    } fmiMECallbackFunctions;
	
	FMIComponent fmiInstantiateModel_(FMIString instanceName,
								  FMIString fmuGUID,
								  fmiMECallbackFunctions functions,
								  FMIBoolean loggingOn);
'''



import os
path = "../../../TwoMasses/sources/"
c_list = [path+f for f in os.listdir(path) if f.endswith('.c')]
h_list = ["#include \"" + f + "\"\n" for f in os.listdir(path) if f.endswith('.h')]
SRC = ""
for h in h_list:
    SRC += h

# print(SRC)


c_list = [
	"../../../TwoMasses/sources/fmiCommonFunctions_int.c",
	"../../../TwoMasses/sources/fmiMEFunctions_int.c"
]
# "../../../TwoMasses/sources/fmi2Functions.c",
# "../../../TwoMasses/sources/fmi2Instantiate.c",


ffibuilder = FFI()
ffibuilder.cdef(CDEF)
ffibuilder.set_source(
    "_test", SRC,
    sources = c_list,
    include_dirs = [path, "../../../FMPy/fmpy/c-code/"],
)   



if __name__ == "__main__":
    ffibuilder.compile(verbose = True)
    
    
    
    

wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/llvm-13.0.0.src.tar.xz
wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang-13.0.0.src.tar.xz
tar xf llvm-13.0.0.src.tar.xz && tar xf clang-13.0.0.src.tar.xz
ln -s $PWD/clang-13.0.0.src llvm-13.0.0.src/tools/clang
cd llvm-13.0.0.src && mkdir build && cd build && cmake .. -G "Ninja" \
    -DCMAKE_BUILD_TYPE=release -DLLVM_BUILD_EXAMPLES=OFF \
    -DBUILD_SHARED_LIBS=OFF -DLLVM_BUILD_TOOLS=ON \
    -DLLVM_ENABLE_BINDINGS=OFF -DLLVM_ENABLE_THREADS=OFF \
    -DLLVM_ENABLE_TERMINFO=OFF -DLLVM_ENABLE_LIBEDIT=OFF \
    -DLLVM_ENABLE_ZLIB=OFF
ninja -j8