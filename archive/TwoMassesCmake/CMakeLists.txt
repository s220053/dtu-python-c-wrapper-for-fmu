cmake_minimum_required (VERSION 3.15)

set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")

set (MODEL_NAME TwoMasses)
set (MODEL_IDENTIFIER TwoMasses)
set (FMI_VERSION 2)

project (${MODEL_NAME})

if (${FMI_VERSION} EQUAL 3)
    if (WIN32)
        set(FMI_PLATFORM windows)
    elseif (APPLE)
        set(FMI_PLATFORM darwin)
    else ()
        set(FMI_PLATFORM linux)
    endif ()

    if ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set (FMI_PLATFORM "x86_64-${FMI_PLATFORM}")
    else ()
        set (FMI_PLATFORM "x86-${FMI_PLATFORM}")
    endif ()
else ()
    if (WIN32)
        set(FMI_PLATFORM win)
    elseif (APPLE)
        set(FMI_PLATFORM darwin)
    else ()
        set(FMI_PLATFORM linux)
    endif ()

    if ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set (FMI_PLATFORM ${FMI_PLATFORM}64)
    else ()
        set (FMI_PLATFORM ${FMI_PLATFORM}32)
    endif ()
endif ()

add_library(${MODEL_IDENTIFIER} SHARED modelDescription.xml sources/all.c)

target_compile_definitions(${MODEL_IDENTIFIER} PUBLIC CO_SIMULATION MODEL_EXCHANGE FMI3_OVERRIDE_FUNCTION_PREFIX)

if (MSVC)
    target_compile_definitions(${MODEL_IDENTIFIER} PUBLIC _CRT_SECURE_NO_WARNINGS)
endif ()

target_include_directories(${MODEL_IDENTIFIER} PUBLIC
    "D:/app/python/lib/site-packages/fmpy/c-code"
    ${CMAKE_CURRENT_SOURCE_DIR}/sources
)

set_target_properties(${MODEL_IDENTIFIER} PROPERTIES
    PREFIX ""
    RUNTIME_OUTPUT_DIRECTORY         "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    RUNTIME_OUTPUT_DIRECTORY_DEBUG   "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    LIBRARY_OUTPUT_DIRECTORY         "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    LIBRARY_OUTPUT_DIRECTORY_DEBUG   "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    ARCHIVE_OUTPUT_DIRECTORY         "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    ARCHIVE_OUTPUT_DIRECTORY_DEBUG   "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
    ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/binaries/${FMI_PLATFORM}"
)

if (WIN32)
    # needed for Dymola FMUs
    target_link_libraries(${MODEL_IDENTIFIER} shlwapi.lib)
endif ()

add_custom_command(TARGET ${MODEL_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E tar cfv "${CMAKE_CURRENT_SOURCE_DIR}/${MODEL_NAME}.fmu" --format=zip
    "modelDescription.xml"
    "binaries/win32/TwoMasses.dll"
    "binaries/win64/TwoMasses.dll"
    "documentation/index.html"
    "documentation/LICENSE_CVODE.txt"
    "documentation/LICENSE_f2c.txt"
    "documentation/LICENSE_FMI.txt"
    "documentation/LICENSE_HDF5.txt"
    "documentation/LICENSE_LAPACK.txt"
    "documentation/LICENSE_ModelicaFFT.txt"
    "documentation/LICENSE_ModelicaInternal.txt"
    "documentation/LICENSE_ModelicaIO.txt"
    "documentation/LICENSE_ModelicaMatIO.txt"
    "documentation/LICENSE_ModelicaRandom.txt"
    "documentation/LICENSE_ModelicaStandardTables.txt"
    "documentation/LICENSE_ModelicaStandardTablesUsertab.txt"
    "documentation/LICENSE_ModelicaStrings.txt"
    "documentation/LICENSE_ModelicaUtilities.txt"
    "documentation/LICENSE_MSL.txt"
    "documentation/LICENSE_snprintf.txt"
    "documentation/LICENSE_stdint_msvc.txt"
    "documentation/LICENSE_SUNDIALS.txt"
    "documentation/LICENSE_SuperLU_MT.txt"
    "documentation/LICENSE_TPL.txt"
    "documentation/LICENSE_uthash.txt"
    "documentation/LICENSE_win32_dirent.txt"
    "documentation/LICENSE_ZLIB.txt"
    "documentation/_main.html"
    "documentation/_opensource.html"
    "sources/adymosim.h"
    "sources/all.c"
    "sources/amach.h"
    "sources/amat.h"
    "sources/assumption.h"
    "sources/atraj.h"
    "sources/bloutil.h"
    "sources/cexch.h"
    "sources/conf.h"
    "sources/csvutil.c"
    "sources/csvutil.h"
    "sources/cvode/cvode.h"
    "sources/cvode/cvode_dense.h"
    "sources/cvode/cvode_direct.h"
    "sources/cvode/LICENSE"
    "sources/cvode.c"
    "sources/cvode_dense.c"
    "sources/cvode_direct.c"
    "sources/cvode_direct_impl.h"
    "sources/cvode_impl.h"
    "sources/cvode_io.c"
    "sources/delay.c"
    "sources/delay.h"
    "sources/dlldata.h"
    "sources/dlldata_impl.h"
    "sources/dsblock.h"
    "sources/dsblock1.c"
    "sources/dsblock2.c"
    "sources/dsblock3.c"
    "sources/dsblock4.c"
    "sources/dsblock5.c"
    "sources/dsblock6.c"
    "sources/dsdefs.h"
    "sources/dse_macros.h"
    "sources/dsmodel.c"
    "sources/dsmodel_fmu.h"
    "sources/dsutil.h"
    "sources/dymf2c.c"
    "sources/dymosim.h"
    "sources/dymtable.c"
    "sources/dymtable.h"
    "sources/dymutil.h"
    "sources/exact-int.h"
    "sources/extendedIncludes.h"
    "sources/f2c.h"
    "sources/fmi2Functions.c"
    "sources/fmi2Functions_fwd.h"
    "sources/fmi2Import.h"
    "sources/fmi3Functions.c"
    "sources/fmi3Functions_fwd.h"
    "sources/fmi3Import.h"
    "sources/fmiCommonFunctions_int.c"
    "sources/fmiCoSimFunctions_int.c"
    "sources/fmiFunctions.c"
    "sources/fmiFunctions_.h"
    "sources/fmiFunctions_1.0_fwd.h"
    "sources/fmiFunctions_fwd.h"
    "sources/fmiImportUtils.h"
    "sources/fmiMEFunctions_int.c"
    "sources/fmiModelFunctions_.h"
    "sources/fmiModelIdentifier.h"
    "sources/fmiPlatformTypes_.h"
    "sources/FMIversionPrefix.h"
    "sources/gconstructor.h"
    "sources/GenerateResultInNonDymosim.h"
    "sources/integration.c"
    "sources/integration.h"
    "sources/jac.c"
    "sources/jac.h"
    "sources/libdssetup.h"
    "sources/localeless.cpp"
    "sources/localeless.h"
    "sources/matrixop.c"
    "sources/matrixop.h"
    "sources/matrixop1.h"
    "sources/matutil.c"
    "sources/matutil.h"
    "sources/memdebug.c"
    "sources/memdebug.h"
    "sources/mman.h"
    "sources/mmap.c"
    "sources/ModelicaFFT.c"
    "sources/ModelicaFFT.h"
    "sources/ModelicaInternal.c"
    "sources/ModelicaInternal.h"
    "sources/ModelicaIO.c"
    "sources/ModelicaIO.h"
    "sources/ModelicaMatIO.c"
    "sources/ModelicaMatIO.h"
    "sources/ModelicaRandom.c"
    "sources/ModelicaRandom.h"
    "sources/ModelicaStandardTables.c"
    "sources/ModelicaStandardTables.h"
    "sources/ModelicaStandardTablesUsertab.c"
    "sources/ModelicaStrings.c"
    "sources/ModelicaStrings.h"
    "sources/ModelicaUtilities.h"
    "sources/moutil.c"
    "sources/nvector/nvector_serial.h"
    "sources/nvector_serial.c"
    "sources/result.h"
    "sources/safe-math.h"
    "sources/snprintf.c"
    "sources/sprwat.h"
    "sources/ss2dym_rt_omp.c"
    "sources/stdint_msvc.h"
    "sources/sundials/sundials_config.h"
    "sources/sundials/sundials_dense.h"
    "sources/sundials/sundials_direct.h"
    "sources/sundials/sundials_math.h"
    "sources/sundials/sundials_nvector.h"
    "sources/sundials/sundials_types.h"
    "sources/sundials_dense.c"
    "sources/sundials_direct.c"
    "sources/sundials_math.c"
    "sources/sundials_nvector.c"
    "sources/tpl.c"
    "sources/tpl.h"
    "sources/types.h"
    "sources/userdefs.h"
    "sources/userfunc.c"
    "sources/usertab.h"
    "sources/uthash.h"
    "sources/util.c"
    "sources/util.h"
    "sources/win32_dirent.c"
    "sources/win32_dirent.h"
    "binaries"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
