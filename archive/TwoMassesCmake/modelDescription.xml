<?xml version="1.0" encoding="UTF-8"?>
<fmiModelDescription
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  fmiVersion="2.0"
  modelName="TwoMasses"
  guid="{17f62517-d3fa-44e2-9eae-e2218a143339}"
  description="Simple conduction demo"
  generationTool="Dymola Version 2023x, 2022-10-07"
  generationDateAndTime="2023-06-21T17:39:08Z"
  variableNamingConvention="structured"
  numberOfEventIndicators="0">
  <ModelExchange
    modelIdentifier="TwoMasses"
    completedIntegratorStepNotNeeded="true"
    canNotUseMemoryManagementFunctions="true"
    canGetAndSetFMUstate="true"
    canSerializeFMUstate="true"
    providesDirectionalDerivative="true">
    <SourceFiles>
      <File
        name="all.c"/>
    </SourceFiles>
  </ModelExchange>
  <CoSimulation
    modelIdentifier="TwoMasses"
    canHandleVariableCommunicationStepSize="true"
    canInterpolateInputs="true"
    maxOutputDerivativeOrder="1"
    canNotUseMemoryManagementFunctions="true"
    canGetAndSetFMUstate="true"
    canSerializeFMUstate="true"
    providesDirectionalDerivative="true">
    <SourceFiles>
      <File
        name="all.c"/>
    </SourceFiles>
  </CoSimulation>
  <UnitDefinitions>
    <Unit
      name="1/K">
      <BaseUnit K="-1"/>
    </Unit>
    <Unit
      name="J/K">
      <BaseUnit kg="1"
        m="2"
        s="-2"
        K="-1"/>
    </Unit>
    <Unit
      name="K">
      <BaseUnit K="1"/>
      <DisplayUnit
        name="degC"
        offset="-273.15"/>
    </Unit>
    <Unit
      name="K/s">
      <BaseUnit s="-1"
        K="1"/>
    </Unit>
    <Unit
      name="W">
      <BaseUnit kg="1"
        m="2"
        s="-3"/>
    </Unit>
    <Unit
      name="W/K">
      <BaseUnit kg="1"
        m="2"
        s="-3"
        K="-1"/>
    </Unit>
    <Unit
      name="degC">
      <BaseUnit K="1"
        offset="273.15"/>
    </Unit>
  </UnitDefinitions>
  <TypeDefinitions>
    <SimpleType
      name="Modelica.Blocks.Interfaces.RealInput">
      <Real/>
    </SimpleType>
    <SimpleType
      name="Modelica.Blocks.Interfaces.RealOutput">
      <Real/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.HeatCapacity">
      <Real
        quantity="HeatCapacity"
        unit="J/K"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.HeatFlowRate">
      <Real
        quantity="Power"
        unit="W"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.LinearTemperatureCoefficient">
      <Real
        quantity="LinearTemperatureCoefficient"
        unit="1/K"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.Temperature">
      <Real
        quantity="ThermodynamicTemperature"
        unit="K"
        displayUnit="degC"
        min="0.0"
        nominal="300.0"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.TemperatureDifference">
      <Real
        quantity="ThermodynamicTemperature"
        unit="K"
        relativeQuantity="true"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.TemperatureSlope">
      <Real
        quantity="TemperatureSlope"
        unit="K/s"/>
    </SimpleType>
    <SimpleType
      name="Modelica.Units.SI.ThermalConductance">
      <Real
        quantity="ThermalConductance"
        unit="W/K"/>
    </SimpleType>
  </TypeDefinitions>
  <DefaultExperiment startTime="0.0"
    stopTime="1.0"
    tolerance="0.0001"/>
  <ModelVariables>
    <!-- Index for next variable = 1 -->
    <ScalarVariable
      name="T_final_K"
      valueReference="100663296"
      description="Projected final temperature"
      causality="calculatedParameter"
      variability="fixed">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 2 -->
    <ScalarVariable
      name="mass1.C"
      valueReference="16777216"
      description="Heat capacity of element (= cp*m)"
      causality="parameter"
      variability="tunable">
      <Real
        declaredType="Modelica.Units.SI.HeatCapacity"
        start="15"/>
    </ScalarVariable>
    <!-- Index for next variable = 3 -->
    <ScalarVariable
      name="mass1.T"
      valueReference="33554432"
      description="Temperature of element"
      initial="exact">
      <Real
        declaredType="Modelica.Units.SI.Temperature"
        start="373.15"/>
    </ScalarVariable>
    <!-- Index for next variable = 4 -->
    <ScalarVariable
      name="der(mass1.T)"
      valueReference="587202560"
      description="der(Temperature of element)">
      <Real
        unit="K/s"
        derivative="3"/>
    </ScalarVariable>
    <!-- Index for next variable = 5 -->
    <ScalarVariable
      name="mass1.der_T"
      valueReference="587202560"
      description="Time derivative of temperature (= der(T))">
      <Real
        declaredType="Modelica.Units.SI.TemperatureSlope"/>
    </ScalarVariable>
    <!-- Index for next variable = 6 -->
    <ScalarVariable
      name="mass1.port.T"
      valueReference="33554432"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 7 -->
    <ScalarVariable
      name="mass1.port.Q_flow"
      valueReference="637534210"
      description="Heat flow rate (positive if flowing from outside into the component)">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
    <!-- Index for next variable = 8 -->
    <ScalarVariable
      name="mass2.C"
      valueReference="16777217"
      description="Heat capacity of element (= cp*m)"
      causality="parameter"
      variability="tunable">
      <Real
        declaredType="Modelica.Units.SI.HeatCapacity"
        start="15"/>
    </ScalarVariable>
    <!-- Index for next variable = 9 -->
    <ScalarVariable
      name="mass2.T"
      valueReference="33554433"
      description="Temperature of element"
      initial="exact">
      <Real
        declaredType="Modelica.Units.SI.Temperature"
        start="273.15"/>
    </ScalarVariable>
    <!-- Index for next variable = 10 -->
    <ScalarVariable
      name="der(mass2.T)"
      valueReference="587202561"
      description="der(Temperature of element)">
      <Real
        unit="K/s"
        derivative="9"/>
    </ScalarVariable>
    <!-- Index for next variable = 11 -->
    <ScalarVariable
      name="mass2.der_T"
      valueReference="587202561"
      description="Time derivative of temperature (= der(T))">
      <Real
        declaredType="Modelica.Units.SI.TemperatureSlope"/>
    </ScalarVariable>
    <!-- Index for next variable = 12 -->
    <ScalarVariable
      name="mass2.port.T"
      valueReference="33554433"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 13 -->
    <ScalarVariable
      name="mass2.port.Q_flow"
      valueReference="637534211"
      description="Heat flow rate (positive if flowing from outside into the component)">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
    <!-- Index for next variable = 14 -->
    <ScalarVariable
      name="conduction.Q_flow"
      valueReference="637534212"
      description="Heat flow rate from port_a -&gt; port_b">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
    <!-- Index for next variable = 15 -->
    <ScalarVariable
      name="conduction.dT"
      valueReference="637534213"
      description="port_a.T - port_b.T">
      <Real
        declaredType="Modelica.Units.SI.TemperatureDifference"/>
    </ScalarVariable>
    <!-- Index for next variable = 16 -->
    <ScalarVariable
      name="conduction.port_a.T"
      valueReference="33554432"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 17 -->
    <ScalarVariable
      name="conduction.port_a.Q_flow"
      valueReference="637534212"
      description="Heat flow rate (positive if flowing from outside into the component)">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
    <!-- Index for next variable = 18 -->
    <ScalarVariable
      name="conduction.port_b.T"
      valueReference="33554433"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 19 -->
    <ScalarVariable
      name="conduction.port_b.Q_flow"
      valueReference="637534210"
      description="Heat flow rate (positive if flowing from outside into the component)">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
    <!-- Index for next variable = 20 -->
    <ScalarVariable
      name="conduction.G"
      valueReference="16777218"
      description="Constant thermal conductance of material"
      causality="parameter"
      variability="tunable">
      <Real
        declaredType="Modelica.Units.SI.ThermalConductance"
        start="10"/>
    </ScalarVariable>
    <!-- Index for next variable = 21 -->
    <ScalarVariable
      name="Tsensor1.T"
      valueReference="335544320"
      description="Absolute temperature in degree Celsius as output signal">
      <Real
        declaredType="Modelica.Blocks.Interfaces.RealOutput"
        unit="degC"/>
    </ScalarVariable>
    <!-- Index for next variable = 22 -->
    <ScalarVariable
      name="Tsensor1.port.T"
      valueReference="33554432"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 23 -->
    <ScalarVariable
      name="Tsensor1.port.Q_flow"
      valueReference="100663297"
      description="Heat flow rate (positive if flowing from outside into the component)"
      variability="constant">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"
        start="0"/>
    </ScalarVariable>
    <!-- Index for next variable = 24 -->
    <ScalarVariable
      name="u"
      valueReference="352321536"
      causality="input">
      <Real
        declaredType="Modelica.Blocks.Interfaces.RealInput"
        unit="W"
        start="0.0"/>
    </ScalarVariable>
    <!-- Index for next variable = 25 -->
    <ScalarVariable
      name="y"
      valueReference="335544320"
      causality="output">
      <Real
        declaredType="Modelica.Blocks.Interfaces.RealOutput"
        unit="degC"/>
    </ScalarVariable>
    <!-- Index for next variable = 26 -->
    <ScalarVariable
      name="prescribedHeatFlow.T_ref"
      valueReference="16777219"
      description="Reference temperature"
      causality="parameter"
      variability="tunable">
      <Real
        declaredType="Modelica.Units.SI.Temperature"
        start="293.15"/>
    </ScalarVariable>
    <!-- Index for next variable = 27 -->
    <ScalarVariable
      name="prescribedHeatFlow.alpha"
      valueReference="16777220"
      description="Temperature coefficient of heat flow rate"
      causality="parameter"
      variability="tunable">
      <Real
        declaredType="Modelica.Units.SI.LinearTemperatureCoefficient"
        start="0"/>
    </ScalarVariable>
    <!-- Index for next variable = 28 -->
    <ScalarVariable
      name="prescribedHeatFlow.Q_flow"
      valueReference="436207616">
      <Real
        declaredType="Modelica.Blocks.Interfaces.RealInput"
        unit="W"/>
    </ScalarVariable>
    <!-- Index for next variable = 29 -->
    <ScalarVariable
      name="prescribedHeatFlow.port.T"
      valueReference="33554433"
      description="Port temperature">
      <Real
        declaredType="Modelica.Units.SI.Temperature"/>
    </ScalarVariable>
    <!-- Index for next variable = 30 -->
    <ScalarVariable
      name="prescribedHeatFlow.port.Q_flow"
      valueReference="637534214"
      description="Heat flow rate (positive if flowing from outside into the component)">
      <Real
        declaredType="Modelica.Units.SI.HeatFlowRate"/>
    </ScalarVariable>
  </ModelVariables>
  <ModelStructure>
    <Outputs>
      <Unknown index="25"
        dependencies="3"
        dependenciesKind="fixed"/>
    </Outputs>
    <Derivatives>
      <Unknown index="4"
        dependencies="3 9"
        dependenciesKind="fixed fixed"/>
      <Unknown index="10"
        dependencies="3 9 24"
        dependenciesKind="fixed dependent dependent"/>
    </Derivatives>
    <InitialUnknowns>
      <Unknown index="1"
        dependencies="2 3 8 9"
        dependenciesKind="dependent dependent dependent dependent"/>
      <Unknown index="4"
        dependencies="2 3 9 20"
        dependenciesKind="dependent dependent dependent dependent"/>
      <Unknown index="10"
        dependencies="3 8 9 20 24 26 27"
        dependenciesKind="dependent dependent dependent dependent dependent dependent dependent"/>
      <Unknown index="25"
        dependencies="3"
        dependenciesKind="dependent"/>
    </InitialUnknowns>
  </ModelStructure>
</fmiModelDescription>
