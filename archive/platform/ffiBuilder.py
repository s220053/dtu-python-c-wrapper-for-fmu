from cffi import FFI


CDEF = '''\
    const char* fmi2GetTypesPlatform();
    const char* fmi2GetVersion();
'''


SRC = '''\
    #include "platform.h"
    
'''


ffibuilder = FFI()
ffibuilder.cdef(CDEF)
ffibuilder.set_source(
    "_test", SRC,
)   

if __name__ == "__main__":
    ffibuilder.compile(verbose = True)
