from _test.lib import fmi2GetVersion,fmi2GetTypesPlatform
import cffi

to_python_string = lambda c_string: cffi.FFI().string(c_string).decode()

print("version:", to_python_string(fmi2GetVersion()), 
      "\nplatform:", to_python_string(fmi2GetTypesPlatform()))
