#define DYMOLA_STATIC
#define fmiPlatform "default"
#define fmiVersion "2.0"

DYMOLA_STATIC const char* fmiGetTypesPlatform_()
{
	return fmiPlatform;
}

DYMOLA_STATIC const char* fmiGetVersion_()
{
	return fmiVersion;
}

const char* fmi2GetTypesPlatform()
{
	return fmiGetTypesPlatform_();
}

const char* fmi2GetVersion()
{
	return fmiGetVersion_();
}
